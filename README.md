# she



# add docker support

# 问题
docker环境中app连不上mysql数据库，地址使用127.0.0.1。
###报错信息
com.mysql.jdbc.exceptions.jdbc4.CommunicationsException: Communications link failure
The last packet sent successfully to the server was 0 milliseconds ago. The driver has not received any packets from the server.

描述：因为app和db是在两个独立容器中，app连接db地址写成127.0.0.1显然是不对的。
解决方案，应该是要找到两个容器各自的ip地址。
参考：https://blog.csdn.net/qq_34021712/article/details/75948566

### 创建自定义网络 
docker network create --subnet=172.172.0.0/24 docker-br0