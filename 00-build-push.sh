
# 编译打包
mvn clean package

# 删除已存在的本地镜像
docker rmi -f licerlee/she-mybatis:latest

# 构建镜像
docker build . -t licerlee/she-mybatis:latest

# 登陆hub
docker login -u licerlee

# 推送镜像
docker push licerlee/she-mybatis:latest

# 退出登陆
# docker logout

# 清理构建
mvn clean