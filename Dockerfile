FROM tomcat:8

ENV TZ=Asia/Shanghai

# Comment
RUN echo 'build start....'


#RUN mvn clean package


ADD target/she-mybatis.war /usr/local/tomcat/webapps/she-mybatis.war

#RUN pip install -r requirements.txt

#EXPOSE 8081

CMD ["catalina.sh", "run"]
