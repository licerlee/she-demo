


# 备注

 使用lombok插件entity的getter/setter方法与mybatis生成方式有区别，
数据库字段命名应避免以下规则：

| 数据库字段 | lombok方式 | mybatis方式 | 
| - | :-: | -: | 
| u_name | getUName()| getuName() | 
