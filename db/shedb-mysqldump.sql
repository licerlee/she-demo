-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: shedb
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `shedb`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `shedb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `shedb`;

--
-- Table structure for table `biz_account`
--

DROP TABLE IF EXISTS `biz_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `biz_account` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYPE_ID` int(11) DEFAULT NULL,
  `ACCOUNT_STR` varchar(1024) NOT NULL,
  `PASSWD_STR` varchar(1024) NOT NULL,
  `NOTE` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_6` (`TYPE_ID`),
  CONSTRAINT `FK_Reference_6` FOREIGN KEY (`TYPE_ID`) REFERENCES `biz_account_type` (`ACCOUNT_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='ACCOUNT';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biz_account`
--

LOCK TABLES `biz_account` WRITE;
/*!40000 ALTER TABLE `biz_account` DISABLE KEYS */;
INSERT INTO `biz_account` VALUES (1,46,'3232','323','2323'),(2,45,'22','22','22'),(3,11,'1','1','1'),(4,46,'fwe','fwe','few'),(5,46,'323','32','32'),(6,44,'5435','4534','54353');
/*!40000 ALTER TABLE `biz_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `biz_account_type`
--

DROP TABLE IF EXISTS `biz_account_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `biz_account_type` (
  `ACCOUNT_TYPE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACCOUNT_TYPE_CODE` varchar(20) NOT NULL,
  `ACCOUNT_TYPE_NAME` varchar(100) NOT NULL,
  PRIMARY KEY (`ACCOUNT_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COMMENT='ACCOUNT_TYPE';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biz_account_type`
--

LOCK TABLES `biz_account_type` WRITE;
/*!40000 ALTER TABLE `biz_account_type` DISABLE KEYS */;
INSERT INTO `biz_account_type` VALUES (6,'1001','天猫'),(7,'1002','QQ'),(8,'1003','淘宝'),(9,'1004','Google'),(10,'1005','百度'),(11,'1006','CSDN'),(12,'1007','雅虎'),(13,'1008','微软'),(14,'1009','魅族'),(15,'1010','小米'),(16,'1011','华为'),(17,'1012','锤子'),(18,'1013','乐视'),(19,'1014','一加'),(20,'1015','酷派'),(21,'1016','360'),(22,'1017','中兴'),(23,'1018','阿里巴巴'),(24,'1019','京东'),(25,'1020','苏宁'),(26,'1021','国美'),(27,'1022','亚马逊'),(28,'1023','当当网'),(29,'1024','华硕'),(30,'1025','小天鹅'),(31,'1026','花生壳'),(32,'1027','迅雷'),(33,'1028','微博'),(34,'1029','gitHub'),(35,'1030','Facebook'),(36,'1030','Facebook'),(37,'1031','Email'),(38,'1032','其他'),(39,'1033','支付宝'),(40,'1034','12306'),(41,'1035','搬瓦工vps'),(42,'1036','cnblog'),(43,'1037','拉钩网'),(44,'1038','paypal'),(45,'1039','自定义12'),(46,'1040','viishow_cn'),(50,'3','33'),(51,'1','1'),(52,'1','2'),(53,'12','12'),(54,'22','3');
/*!40000 ALTER TABLE `biz_account_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `biz_attachment`
--

DROP TABLE IF EXISTS `biz_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `biz_attachment` (
  `FILE_ID` varchar(32) NOT NULL,
  `FILE_NAME` varchar(1024) DEFAULT NULL,
  `FILE_GROUP` varchar(1024) DEFAULT NULL,
  `ORGN_NAME` varchar(1024) DEFAULT NULL,
  `FILE_SIZE` varchar(1024) DEFAULT NULL,
  `FILE_EXT` varchar(1024) DEFAULT NULL,
  `FILE_PATH` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ATTACHMENT';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biz_attachment`
--

LOCK TABLES `biz_attachment` WRITE;
/*!40000 ALTER TABLE `biz_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `biz_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `biz_task`
--

DROP TABLE IF EXISTS `biz_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `biz_task` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `TASK_TITLE` varchar(255) DEFAULT '' COMMENT '任务标题',
  `TASK_CONTENT` varchar(255) DEFAULT '' COMMENT '任务内容',
  `FINISH_STATUS` int(11) unsigned zerofill DEFAULT NULL COMMENT '完成状态',
  `PLAN_START` datetime DEFAULT NULL COMMENT '计划开始日期',
  `PLAN_FINISH` datetime DEFAULT NULL COMMENT '计划完成日期',
  `ACTUAL_START` datetime DEFAULT NULL COMMENT '实际开始日期',
  `ACTUAL_FINISH` datetime DEFAULT NULL COMMENT '实际完成日期',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biz_task`
--

LOCK TABLES `biz_task` WRITE;
/*!40000 ALTER TABLE `biz_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `biz_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_acc_his`
--

DROP TABLE IF EXISTS `sys_acc_his`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_acc_his` (
  `ID` varchar(32) NOT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `OPT_TIME` datetime DEFAULT NULL,
  `IP` varchar(255) DEFAULT '',
  `HOST` varchar(255) DEFAULT '',
  `USER_AGENT` varchar(255) DEFAULT '',
  `COOKIE` varchar(255) DEFAULT '',
  `ACCEPT` varchar(255) DEFAULT '',
  `ACCEPT_ENCODING` varchar(255) DEFAULT '',
  `ACCEPT_LANGUAGE` varchar(255) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_acc_his`
--

LOCK TABLES `sys_acc_his` WRITE;
/*!40000 ALTER TABLE `sys_acc_his` DISABLE KEYS */;
INSERT INTO `sys_acc_his` VALUES ('18',NULL,NULL,'admin','admin','系统管理-liwc','18',NULL,NULL,NULL);
/*!40000 ALTER TABLE `sys_acc_his` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_code`
--

DROP TABLE IF EXISTS `sys_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_code` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(255) DEFAULT '',
  `VALUE` varchar(255) DEFAULT '',
  `CATEG` varchar(255) DEFAULT '',
  `CATEG_NAME` varchar(255) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_code`
--

LOCK TABLES `sys_code` WRITE;
/*!40000 ALTER TABLE `sys_code` DISABLE KEYS */;
INSERT INTO `sys_code` VALUES (1,'icon-settings','设置','icon','图标'),(3,'icon-business','业务','icon','图标'),(4,'icon-chart','图表','icon','图标'),(5,'icon-base','基础','icon','图标'),(6,'F','女','sex','性别'),(7,'M','男','sex','性别'),(9,'Unkown','未知','sex','性别'),(10,'icon-blank','空白','icon','图标'),(11,'icon-add','新增','icon','图标'),(12,'1','一级','menuLevel','菜单等级'),(13,'2','二级','menuLevel','菜单等级'),(14,'icon-base','缺省','icon','图标'),(15,'0','缺省','UserStatus','用户状态'),(16,'1','正常','UserStatus','用户状态'),(17,'2','冻结','UserStatus','用户状态'),(18,'0','缺省','RoleStatus','角色状态'),(19,'1','正常','RoleStatus','角色状态'),(20,'2','冻结','RoleStatus','角色状态');
/*!40000 ALTER TABLE `sys_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_menu`
--

DROP TABLE IF EXISTS `sys_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_menu` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `MENU_NAME` varchar(255) DEFAULT '',
  `MENU_URL` varchar(255) DEFAULT '',
  `MENU_ORDER` int(11) DEFAULT NULL,
  `MENU_PARENT` int(11) DEFAULT NULL,
  `MENU_LEVEL` int(11) DEFAULT NULL,
  `MENU_TYPE` varchar(255) DEFAULT '',
  `MENU_ICON` varchar(255) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_menu`
--

LOCK TABLES `sys_menu` WRITE;
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
INSERT INTO `sys_menu` VALUES (1,'Dashboard1','',1,NULL,1,'dir','icon-business'),(2,'Layout Options',NULL,2,NULL,1,'dir','icon-business'),(3,'Widgets',NULL,3,NULL,1,'dir','icon-base'),(4,'Charts',NULL,4,NULL,1,'dir','icon-chart'),(16,'Dashboard v1','/dashboard',1,1,2,'page',NULL),(17,'Dashboard v2','/dashboard2',2,1,2,'page',NULL),(18,'顶部导航',NULL,1,2,2,'page',NULL),(19,'盒子',NULL,2,2,2,'page',NULL),(20,'定位',NULL,3,2,2,'page',NULL),(21,'可折叠边栏',NULL,4,2,2,'page',NULL),(22,'JS图表',NULL,1,4,2,'page',NULL),(23,' Morris图表',NULL,2,4,2,'page',NULL),(25,'Echarts',NULL,3,4,2,'page',NULL),(26,'系统管理',NULL,0,NULL,1,'dir','icon-settings'),(27,'菜单管理(treegrid)','/system/menu/index',1,26,2,'page','icon-base'),(36,'菜单管理(datagrid)','/system/menu/indexDatagrid',2,26,2,'page','icon-base'),(37,'一级目录','',16,NULL,1,'dir','icon-base'),(38,'页面1','/system/test',1,37,2,'page',''),(39,'zTree','',17,NULL,1,'dir','icon-base'),(40,'api','/static/plugins/zTree_v3/api/API_cn.html',1,39,2,'page',''),(41,'demo','/static/plugins/zTree_v3/demo/cn/index.html',2,39,2,'page',''),(68,'数据字典','/system/code/index',3,26,2,'page','icon-base'),(69,'角色管理','/system/role/index',4,26,2,'page','icon-base'),(70,'用户管理','/system/user/index',5,26,2,'page','icon-base'),(71,'部门管理','/system/dept/index',6,26,2,'page','icon-base'),(72,'111','',22,NULL,2,'dir','icon-business'),(74,'33','',NULL,NULL,NULL,'','icon-settings'),(75,'1','',NULL,NULL,NULL,'','icon-business'),(76,'测试一级菜单','',NULL,NULL,NULL,'dir','icon-business'),(77,'我的一级菜单','',NULL,NULL,1,'dir','icon-settings'),(78,'我的二级菜单','/biz/my/index',NULL,77,2,'page','icon-business');
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_param`
--

DROP TABLE IF EXISTS `sys_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_param` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PARAM_CODE` varchar(255) DEFAULT '',
  `PARAM_VALUE` varchar(255) DEFAULT NULL,
  `PARAM_NAME` varchar(255) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_param`
--

LOCK TABLES `sys_param` WRITE;
/*!40000 ALTER TABLE `sys_param` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_param` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `ROLE_CODE` varchar(255) DEFAULT '',
  `ROLE_NAME` varchar(255) DEFAULT '',
  `ROLE_ORDER` int(11) DEFAULT NULL,
  `ROLE_STATUS` int(11) DEFAULT NULL,
  `ROLE_DESC` varchar(255) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` VALUES (18,'100','系统管理员',1,1,'负责系统开发建设'),(19,'200','运维管理员',2,1,'负责后台日程运维'),(22,'300','业务主管',3,1,''),(23,'400','业务员',4,1,'');
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_menu`
--

DROP TABLE IF EXISTS `sys_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role_menu` (
  `ROLE_ID` int(11) NOT NULL,
  `MENU_ID` int(11) NOT NULL,
  PRIMARY KEY (`ROLE_ID`,`MENU_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_menu`
--

LOCK TABLES `sys_role_menu` WRITE;
/*!40000 ALTER TABLE `sys_role_menu` DISABLE KEYS */;
INSERT INTO `sys_role_menu` VALUES (18,1),(18,2),(18,3),(18,26),(18,27),(18,36),(18,37),(18,38),(18,39),(18,40),(18,41),(18,68),(18,69),(18,70),(18,71),(19,26),(19,36),(19,39),(19,40),(19,41),(19,68),(19,69),(19,70),(22,1),(22,2),(22,16),(22,17),(22,18),(22,19),(22,20),(22,21),(22,37),(22,38),(22,39),(22,40),(22,41),(22,77),(22,78),(23,77),(23,78);
/*!40000 ALTER TABLE `sys_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_LOGIN_ID` varchar(255) DEFAULT '',
  `USER_PASSWD` varchar(255) DEFAULT '',
  `USER_NAME` varchar(255) DEFAULT '',
  `USER_STATUS` int(11) DEFAULT NULL,
  `USER_TYPE` int(11) DEFAULT NULL,
  `USER_ROLE` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (18,'admin','21232F297A57A5A743894A0E4A801FC3','系统管理-liwc',1,1,18),(24,'admin-yw1','C4CA4238A0B923820DCC509A6F75849B','liwenchao',1,1,19),(25,'ywy1','C4CA4238A0B923820DCC509A6F75849B','业务员1',1,1,23),(26,'ywzg1','C4CA4238A0B923820DCC509A6F75849B','业务主管1',1,1,22);
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-27 12:02:36
