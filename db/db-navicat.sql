/*
Navicat MySQL Data Transfer

Source Server         : local-she
Source Server Version : 50635
Source Host           : localhost:3306
Source Database       : shedb

Target Server Type    : MYSQL
Target Server Version : 50635
File Encoding         : 65001

Date: 2018-05-28 18:14:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `biz_account`
-- ----------------------------
DROP TABLE IF EXISTS `biz_account`;
CREATE TABLE `biz_account` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYPE_ID` int(11) DEFAULT NULL,
  `ACCOUNT_STR` varchar(1024) NOT NULL,
  `PASSWD_STR` varchar(1024) NOT NULL,
  `NOTE` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_6` (`TYPE_ID`),
  CONSTRAINT `FK_Reference_6` FOREIGN KEY (`TYPE_ID`) REFERENCES `biz_account_type` (`ACCOUNT_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='ACCOUNT';

-- ----------------------------
-- Records of biz_account
-- ----------------------------
INSERT INTO `biz_account` VALUES ('1', '46', '3232', '323', '2323');
INSERT INTO `biz_account` VALUES ('2', '45', '22', '22', '22');
INSERT INTO `biz_account` VALUES ('3', '11', '1', '1', '1');
INSERT INTO `biz_account` VALUES ('4', '46', 'fwe', 'fwe', 'few');
INSERT INTO `biz_account` VALUES ('5', '46', '323', '32', '32');
INSERT INTO `biz_account` VALUES ('6', '44', '5435', '4534', '54353');

-- ----------------------------
-- Table structure for `biz_account_type`
-- ----------------------------
DROP TABLE IF EXISTS `biz_account_type`;
CREATE TABLE `biz_account_type` (
  `ACCOUNT_TYPE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACCOUNT_TYPE_CODE` varchar(20) NOT NULL,
  `ACCOUNT_TYPE_NAME` varchar(100) NOT NULL,
  PRIMARY KEY (`ACCOUNT_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COMMENT='ACCOUNT_TYPE';

-- ----------------------------
-- Records of biz_account_type
-- ----------------------------
INSERT INTO `biz_account_type` VALUES ('6', '1001', '天猫');
INSERT INTO `biz_account_type` VALUES ('7', '1002', 'QQ');
INSERT INTO `biz_account_type` VALUES ('8', '1003', '淘宝');
INSERT INTO `biz_account_type` VALUES ('9', '1004', 'Google');
INSERT INTO `biz_account_type` VALUES ('10', '1005', '百度');
INSERT INTO `biz_account_type` VALUES ('11', '1006', 'CSDN');
INSERT INTO `biz_account_type` VALUES ('12', '1007', '雅虎');
INSERT INTO `biz_account_type` VALUES ('13', '1008', '微软');
INSERT INTO `biz_account_type` VALUES ('14', '1009', '魅族');
INSERT INTO `biz_account_type` VALUES ('15', '1010', '小米');
INSERT INTO `biz_account_type` VALUES ('16', '1011', '华为');
INSERT INTO `biz_account_type` VALUES ('17', '1012', '锤子');
INSERT INTO `biz_account_type` VALUES ('18', '1013', '乐视');
INSERT INTO `biz_account_type` VALUES ('19', '1014', '一加');
INSERT INTO `biz_account_type` VALUES ('20', '1015', '酷派');
INSERT INTO `biz_account_type` VALUES ('21', '1016', '360');
INSERT INTO `biz_account_type` VALUES ('22', '1017', '中兴');
INSERT INTO `biz_account_type` VALUES ('23', '1018', '阿里巴巴');
INSERT INTO `biz_account_type` VALUES ('24', '1019', '京东');
INSERT INTO `biz_account_type` VALUES ('25', '1020', '苏宁');
INSERT INTO `biz_account_type` VALUES ('26', '1021', '国美');
INSERT INTO `biz_account_type` VALUES ('27', '1022', '亚马逊');
INSERT INTO `biz_account_type` VALUES ('28', '1023', '当当网');
INSERT INTO `biz_account_type` VALUES ('29', '1024', '华硕');
INSERT INTO `biz_account_type` VALUES ('30', '1025', '小天鹅');
INSERT INTO `biz_account_type` VALUES ('31', '1026', '花生壳');
INSERT INTO `biz_account_type` VALUES ('32', '1027', '迅雷');
INSERT INTO `biz_account_type` VALUES ('33', '1028', '微博');
INSERT INTO `biz_account_type` VALUES ('34', '1029', 'gitHub');
INSERT INTO `biz_account_type` VALUES ('35', '1030', 'Facebook');
INSERT INTO `biz_account_type` VALUES ('36', '1030', 'Facebook');
INSERT INTO `biz_account_type` VALUES ('37', '1031', 'Email');
INSERT INTO `biz_account_type` VALUES ('38', '1032', '其他');
INSERT INTO `biz_account_type` VALUES ('39', '1033', '支付宝');
INSERT INTO `biz_account_type` VALUES ('40', '1034', '12306');
INSERT INTO `biz_account_type` VALUES ('41', '1035', '搬瓦工vps');
INSERT INTO `biz_account_type` VALUES ('42', '1036', 'cnblog');
INSERT INTO `biz_account_type` VALUES ('43', '1037', '拉钩网');
INSERT INTO `biz_account_type` VALUES ('44', '1038', 'paypal');
INSERT INTO `biz_account_type` VALUES ('45', '1039', '自定义12');
INSERT INTO `biz_account_type` VALUES ('46', '1040', 'viishow_cn');
INSERT INTO `biz_account_type` VALUES ('50', '3', '33');
INSERT INTO `biz_account_type` VALUES ('51', '1', '1');
INSERT INTO `biz_account_type` VALUES ('52', '1', '2');
INSERT INTO `biz_account_type` VALUES ('53', '12', '12');
INSERT INTO `biz_account_type` VALUES ('54', '22', '3');

-- ----------------------------
-- Table structure for `biz_attachment`
-- ----------------------------
DROP TABLE IF EXISTS `biz_attachment`;
CREATE TABLE `biz_attachment` (
  `FILE_ID` varchar(32) NOT NULL,
  `FILE_NAME` varchar(1024) DEFAULT NULL,
  `FILE_GROUP` varchar(1024) DEFAULT NULL,
  `ORGN_NAME` varchar(1024) DEFAULT NULL,
  `FILE_SIZE` varchar(1024) DEFAULT NULL,
  `FILE_EXT` varchar(1024) DEFAULT NULL,
  `FILE_PATH` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ATTACHMENT';

-- ----------------------------
-- Records of biz_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for `biz_task`
-- ----------------------------
DROP TABLE IF EXISTS `biz_task`;
CREATE TABLE `biz_task` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `TASK_TITLE` varchar(255) DEFAULT '' COMMENT '任务标题',
  `TASK_CONTENT` varchar(255) DEFAULT '' COMMENT '任务内容',
  `FINISH_STATUS` int(11) unsigned zerofill DEFAULT NULL COMMENT '完成状态',
  `PLAN_START` datetime DEFAULT NULL COMMENT '计划开始日期',
  `PLAN_FINISH` datetime DEFAULT NULL COMMENT '计划完成日期',
  `ACTUAL_START` datetime DEFAULT NULL COMMENT '实际开始日期',
  `ACTUAL_FINISH` datetime DEFAULT NULL COMMENT '实际完成日期',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of biz_task
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_acc_his`
-- ----------------------------
DROP TABLE IF EXISTS `sys_acc_his`;
CREATE TABLE `sys_acc_his` (
  `ID` varchar(32) NOT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `OPT_TIME` datetime DEFAULT NULL,
  `IP` varchar(255) DEFAULT '',
  `HOST` varchar(255) DEFAULT '',
  `USER_AGENT` varchar(255) DEFAULT '',
  `COOKIE` varchar(255) DEFAULT '',
  `ACCEPT` varchar(255) DEFAULT '',
  `ACCEPT_ENCODING` varchar(255) DEFAULT '',
  `ACCEPT_LANGUAGE` varchar(255) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_acc_his
-- ----------------------------
INSERT INTO `sys_acc_his` VALUES ('18', null, null, 'admin', 'admin', '系统管理-liwc', '18', null, null, null);

-- ----------------------------
-- Table structure for `sys_code`
-- ----------------------------
DROP TABLE IF EXISTS `sys_code`;
CREATE TABLE `sys_code` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(255) DEFAULT '',
  `VALUE` varchar(255) DEFAULT '',
  `CATEG` varchar(255) DEFAULT '',
  `CATEG_NAME` varchar(255) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_code
-- ----------------------------
INSERT INTO `sys_code` VALUES ('1', 'icon-settings', '设置', 'icon', '图标');
INSERT INTO `sys_code` VALUES ('3', 'icon-business', '业务', 'icon', '图标');
INSERT INTO `sys_code` VALUES ('4', 'icon-chart', '图表', 'icon', '图标');
INSERT INTO `sys_code` VALUES ('5', 'icon-base', '基础', 'icon', '图标');
INSERT INTO `sys_code` VALUES ('6', 'F', '女', 'sex', '性别');
INSERT INTO `sys_code` VALUES ('7', 'M', '男', 'sex', '性别');
INSERT INTO `sys_code` VALUES ('9', 'Unkown', '未知', 'sex', '性别');
INSERT INTO `sys_code` VALUES ('10', 'icon-blank', '空白', 'icon', '图标');
INSERT INTO `sys_code` VALUES ('11', 'icon-add', '新增', 'icon', '图标');
INSERT INTO `sys_code` VALUES ('12', '1', '一级', 'menuLevel', '菜单等级');
INSERT INTO `sys_code` VALUES ('13', '2', '二级', 'menuLevel', '菜单等级');
INSERT INTO `sys_code` VALUES ('14', 'icon-base', '缺省', 'icon', '图标');
INSERT INTO `sys_code` VALUES ('15', '0', '缺省', 'UserStatus', '用户状态');
INSERT INTO `sys_code` VALUES ('16', '1', '正常', 'UserStatus', '用户状态');
INSERT INTO `sys_code` VALUES ('17', '2', '冻结', 'UserStatus', '用户状态');
INSERT INTO `sys_code` VALUES ('18', '0', '缺省', 'RoleStatus', '角色状态');
INSERT INTO `sys_code` VALUES ('19', '1', '正常', 'RoleStatus', '角色状态');
INSERT INTO `sys_code` VALUES ('20', '2', '冻结', 'RoleStatus', '角色状态');

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `MENU_NAME` varchar(255) DEFAULT '',
  `MENU_URL` varchar(255) DEFAULT '',
  `MENU_ORDER` int(11) DEFAULT NULL,
  `MENU_PARENT` int(11) DEFAULT NULL,
  `MENU_LEVEL` int(11) DEFAULT NULL,
  `MENU_TYPE` varchar(255) DEFAULT '',
  `MENU_ICON` varchar(255) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', 'Dashboard1', '', '1', null, '1', 'dir', 'icon-business');
INSERT INTO `sys_menu` VALUES ('2', 'Layout Options', null, '2', null, '1', 'dir', 'icon-business');
INSERT INTO `sys_menu` VALUES ('3', 'Widgets', null, '3', null, '1', 'dir', 'icon-base');
INSERT INTO `sys_menu` VALUES ('4', 'Charts', null, '4', null, '1', 'dir', 'icon-chart');
INSERT INTO `sys_menu` VALUES ('16', 'Dashboard v1', '/dashboard', '1', '1', '2', 'page', null);
INSERT INTO `sys_menu` VALUES ('17', 'Dashboard v2', '/dashboard2', '2', '1', '2', 'page', null);
INSERT INTO `sys_menu` VALUES ('18', '顶部导航', null, '1', '2', '2', 'page', null);
INSERT INTO `sys_menu` VALUES ('19', '盒子', null, '2', '2', '2', 'page', null);
INSERT INTO `sys_menu` VALUES ('20', '定位', null, '3', '2', '2', 'page', null);
INSERT INTO `sys_menu` VALUES ('21', '可折叠边栏', null, '4', '2', '2', 'page', null);
INSERT INTO `sys_menu` VALUES ('22', 'JS图表', null, '1', '4', '2', 'page', null);
INSERT INTO `sys_menu` VALUES ('23', ' Morris图表', null, '2', '4', '2', 'page', null);
INSERT INTO `sys_menu` VALUES ('25', 'Echarts', null, '3', '4', '2', 'page', null);
INSERT INTO `sys_menu` VALUES ('26', '系统管理', null, '0', null, '1', 'dir', 'icon-settings');
INSERT INTO `sys_menu` VALUES ('27', '菜单管理(treegrid)', '/system/menu/index', '1', '26', '2', 'page', 'icon-base');
INSERT INTO `sys_menu` VALUES ('36', '菜单管理(datagrid)', '/system/menu/indexDatagrid', '2', '26', '2', 'page', 'icon-base');
INSERT INTO `sys_menu` VALUES ('37', '一级目录', '', '16', null, '1', 'dir', 'icon-base');
INSERT INTO `sys_menu` VALUES ('38', '页面1', '/system/test', '1', '37', '2', 'page', '');
INSERT INTO `sys_menu` VALUES ('39', 'zTree', '', '17', null, '1', 'dir', 'icon-base');
INSERT INTO `sys_menu` VALUES ('40', 'api', '/static/plugins/zTree_v3/api/API_cn.html', '1', '39', '2', 'page', '');
INSERT INTO `sys_menu` VALUES ('41', 'demo', '/static/plugins/zTree_v3/demo/cn/index.html', '2', '39', '2', 'page', '');
INSERT INTO `sys_menu` VALUES ('68', '数据字典', '/system/code/index', '3', '26', '2', 'page', 'icon-base');
INSERT INTO `sys_menu` VALUES ('69', '角色管理', '/system/role/index', '4', '26', '2', 'page', 'icon-base');
INSERT INTO `sys_menu` VALUES ('70', '用户管理', '/system/user/index', '5', '26', '2', 'page', 'icon-base');
INSERT INTO `sys_menu` VALUES ('71', '部门管理', '/system/dept/index', '6', '26', '2', 'page', 'icon-base');
INSERT INTO `sys_menu` VALUES ('72', '111', '', '22', null, '2', 'dir', 'icon-business');
INSERT INTO `sys_menu` VALUES ('74', '33', '', null, null, null, '', 'icon-settings');
INSERT INTO `sys_menu` VALUES ('75', '1', '', null, null, null, '', 'icon-business');
INSERT INTO `sys_menu` VALUES ('76', '测试一级菜单', '', null, null, null, 'dir', 'icon-business');
INSERT INTO `sys_menu` VALUES ('77', '我的一级菜单', '', null, null, '1', 'dir', 'icon-settings');
INSERT INTO `sys_menu` VALUES ('78', '我的二级菜单', '/biz/my/index', null, '77', '2', 'page', 'icon-business');
INSERT INTO `sys_menu` VALUES ('79', '报表统计', '', null, null, '1', 'dir', 'icon-chart');
INSERT INTO `sys_menu` VALUES ('80', '用户活跃趋势统计', '/system/report/userActive', null, '79', '2', 'page', 'icon-chart');

-- ----------------------------
-- Table structure for `sys_param`
-- ----------------------------
DROP TABLE IF EXISTS `sys_param`;
CREATE TABLE `sys_param` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PARAM_CODE` varchar(255) DEFAULT '',
  `PARAM_VALUE` varchar(255) DEFAULT NULL,
  `PARAM_NAME` varchar(255) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_param
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `ROLE_CODE` varchar(255) DEFAULT '',
  `ROLE_NAME` varchar(255) DEFAULT '',
  `ROLE_ORDER` int(11) DEFAULT NULL,
  `ROLE_STATUS` int(11) DEFAULT NULL,
  `ROLE_DESC` varchar(255) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('18', '100', '系统管理员', '1', '1', '负责系统开发建设');
INSERT INTO `sys_role` VALUES ('19', '200', '运维管理员', '2', '1', '负责后台日程运维');
INSERT INTO `sys_role` VALUES ('22', '300', '业务主管', '3', '1', '');
INSERT INTO `sys_role` VALUES ('23', '400', '业务员', '4', '1', '');

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `ROLE_ID` int(11) NOT NULL,
  `MENU_ID` int(11) NOT NULL,
  PRIMARY KEY (`ROLE_ID`,`MENU_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('18', '1');
INSERT INTO `sys_role_menu` VALUES ('18', '2');
INSERT INTO `sys_role_menu` VALUES ('18', '3');
INSERT INTO `sys_role_menu` VALUES ('18', '26');
INSERT INTO `sys_role_menu` VALUES ('18', '27');
INSERT INTO `sys_role_menu` VALUES ('18', '36');
INSERT INTO `sys_role_menu` VALUES ('18', '37');
INSERT INTO `sys_role_menu` VALUES ('18', '38');
INSERT INTO `sys_role_menu` VALUES ('18', '39');
INSERT INTO `sys_role_menu` VALUES ('18', '40');
INSERT INTO `sys_role_menu` VALUES ('18', '41');
INSERT INTO `sys_role_menu` VALUES ('18', '68');
INSERT INTO `sys_role_menu` VALUES ('18', '69');
INSERT INTO `sys_role_menu` VALUES ('18', '70');
INSERT INTO `sys_role_menu` VALUES ('18', '71');
INSERT INTO `sys_role_menu` VALUES ('18', '79');
INSERT INTO `sys_role_menu` VALUES ('18', '80');
INSERT INTO `sys_role_menu` VALUES ('19', '26');
INSERT INTO `sys_role_menu` VALUES ('19', '36');
INSERT INTO `sys_role_menu` VALUES ('19', '39');
INSERT INTO `sys_role_menu` VALUES ('19', '40');
INSERT INTO `sys_role_menu` VALUES ('19', '41');
INSERT INTO `sys_role_menu` VALUES ('19', '68');
INSERT INTO `sys_role_menu` VALUES ('19', '69');
INSERT INTO `sys_role_menu` VALUES ('19', '70');
INSERT INTO `sys_role_menu` VALUES ('22', '1');
INSERT INTO `sys_role_menu` VALUES ('22', '2');
INSERT INTO `sys_role_menu` VALUES ('22', '16');
INSERT INTO `sys_role_menu` VALUES ('22', '17');
INSERT INTO `sys_role_menu` VALUES ('22', '18');
INSERT INTO `sys_role_menu` VALUES ('22', '19');
INSERT INTO `sys_role_menu` VALUES ('22', '20');
INSERT INTO `sys_role_menu` VALUES ('22', '21');
INSERT INTO `sys_role_menu` VALUES ('22', '37');
INSERT INTO `sys_role_menu` VALUES ('22', '38');
INSERT INTO `sys_role_menu` VALUES ('22', '39');
INSERT INTO `sys_role_menu` VALUES ('22', '40');
INSERT INTO `sys_role_menu` VALUES ('22', '41');
INSERT INTO `sys_role_menu` VALUES ('22', '77');
INSERT INTO `sys_role_menu` VALUES ('22', '78');
INSERT INTO `sys_role_menu` VALUES ('23', '77');
INSERT INTO `sys_role_menu` VALUES ('23', '78');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_LOGIN_ID` varchar(255) DEFAULT '',
  `USER_PASSWD` varchar(255) DEFAULT '',
  `USER_NAME` varchar(255) DEFAULT '',
  `USER_STATUS` int(11) DEFAULT NULL,
  `USER_TYPE` int(11) DEFAULT NULL,
  `USER_ROLE` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('18', 'admin', '21232F297A57A5A743894A0E4A801FC3', '系统管理-liwc', '1', '1', '18');
INSERT INTO `sys_user` VALUES ('24', 'admin-yw1', 'C4CA4238A0B923820DCC509A6F75849B', 'liwenchao', '1', '1', '19');
INSERT INTO `sys_user` VALUES ('25', 'ywy1', 'C4CA4238A0B923820DCC509A6F75849B', '业务员1', '1', '1', '23');
INSERT INTO `sys_user` VALUES ('26', 'ywzg1', 'C4CA4238A0B923820DCC509A6F75849B', '业务主管1', '1', '1', '22');
INSERT INTO `sys_user` VALUES ('27', '1', 'C4CA4238A0B923820DCC509A6F75849B', '1', '0', '1', '18');
