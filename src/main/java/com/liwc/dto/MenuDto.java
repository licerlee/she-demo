package com.liwc.dto;

import java.util.List;

import com.liwc.model.SysMenu;
import com.liwc.model.page.PageEasyui;

import lombok.Data;

@Data
public class MenuDto extends PageEasyui{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

	private String menuName;

	private String menuUrl;

	private Integer menuOrder;

	private Integer menuParent;

	private Integer menuLevel;

	private String menuType;

	private String menuIcon;
	
	
	private transient String menuParentName;
	
	// 一对多关联字段
	private List<SysMenu> children;


	private transient Integer roleId;
	
	
}