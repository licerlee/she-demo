package com.liwc.dto;

import java.util.Set;

import com.liwc.model.SysMenu;
import com.liwc.model.SysRole;
import com.liwc.model.page.PageEasyui;

import lombok.Data;

@Data
public class UserDto extends PageEasyui{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

	private String userLoginId;

	private String userPasswd;

	private String userName;

	private Integer userStatus;

	private Integer userType;

	private Integer userRole;

	private Set<SysRole> roles;

	private Set<SysMenu> menus;
	
	
	
}
