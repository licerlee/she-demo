package com.liwc.dto;

import com.liwc.model.page.PageEasyui;

import lombok.Data;

@Data
public class CodeDto extends PageEasyui {

	private Integer id;

	private String code;

	private String value;

	private String categ;

	private String categName;

}
