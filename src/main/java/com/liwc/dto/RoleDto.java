package com.liwc.dto;

import java.util.Set;

import com.liwc.model.SysMenu;
import com.liwc.model.SysMenuTree;
import com.liwc.model.page.PageEasyui;

import lombok.Data;

/**
 * 
 * 模块：基于RBAC的基础权限框架demo
 * 描述：
 * @author liwc
 * @date 2018年3月25日 下午10:06:01
 * @ RoleDto
 */
@Data
public class RoleDto extends PageEasyui{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

	private String roleCode;

	private String roleName;

	private Integer roleOrder;

	private Integer roleStatus;

	private String roleDesc;

	private Set<SysMenu> menus;
	
	private Set<SysMenuTree> menuTrees;
	
	
}
