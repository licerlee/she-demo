package com.liwc.dao;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.liwc.dto.UserDto;
import com.liwc.model.SysMenu;
import com.liwc.model.SysUser;

@MapperScan
public interface SysUserMapper {
	
	int deleteByPrimaryKey(Integer id);

	int insert(SysUser record);

	int insertSelective(SysUser record);

	SysUser selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(SysUser record);

	int updateByPrimaryKey(SysUser record);

	
	List<SysUser> findUserByName(String userName);

	List<SysMenu> findResourcesByUserId(Integer userId);

	List<SysUser> selectAll(UserDto dto);
}