package com.liwc.dao;

import java.util.List;

import com.liwc.dto.CodeDto;
import com.liwc.model.SysCode;

public interface SysCodeMapper {
	
	
	int deleteByPrimaryKey(Integer id);

	int insert(SysCode record);

	int insertSelective(SysCode record);

	SysCode selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(SysCode record);

	int updateByPrimaryKey(SysCode record);

	List<SysCode> selectAll(CodeDto dto);

	List<SysCode> selectRootList();
	
}