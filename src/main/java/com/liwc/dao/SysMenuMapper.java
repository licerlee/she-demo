package com.liwc.dao;

import java.util.List;
import java.util.Map;

import com.liwc.dto.MenuDto;
import com.liwc.model.SysMenu;
import com.liwc.model.SysMenuTree;

public interface SysMenuMapper {
	
	
	int deleteByPrimaryKey(Integer id);

	int insert(SysMenu record);

	int insertSelective(SysMenu record);

	SysMenu selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(SysMenu record);

	int updateByPrimaryKey(SysMenu record);

	/**
	 * 查询所有 附带子菜单
	 * @return
	 */
	List<SysMenu> selectAllWithChild();
	
	/**
	 * 查询所有
	 * @param dto 
	 * @return
	 */
	List<SysMenu> selectAll(MenuDto dto);
	
	/**
	 * 查询树形菜单
	 * @return
	 */
	List<SysMenuTree> selectTreeMenu(MenuDto dto);
	
}