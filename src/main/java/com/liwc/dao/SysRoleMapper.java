package com.liwc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

import com.liwc.dto.RoleDto;
import com.liwc.model.SysMenuTree;
import com.liwc.model.SysRole;

import net.sf.json.JSONArray;

public interface SysRoleMapper {
	
	
	int deleteByPrimaryKey(Integer id);

	int insert(SysRole record);

	int insertSelective(SysRole record);

	SysRole selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(SysRole record);

	int updateByPrimaryKey(SysRole record);

	List<SysRole> selectAll(RoleDto dto);

	int insertRoleMenu(Integer id, SysMenuTree menuTree);

	@Insert("insert into sys_role_menu(role_id, menu_id) values(#{roleId}, #{menuId})")
	void saveRoleMenu(@Param("roleId") Integer id, @Param("menuId") String menuId);
	
}