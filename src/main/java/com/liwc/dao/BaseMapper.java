package com.liwc.dao;

import java.util.List;

import com.liwc.model.SysMenu;

public interface BaseMapper<T> extends com.baomidou.mybatisplus.mapper.BaseMapper<T>{
	
	
	int deleteByPrimaryKey(Integer id);

	int insert(SysMenu record);

	int insertSelective(SysMenu record);

	SysMenu selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(SysMenu record);

	int updateByPrimaryKey(SysMenu record);

	/**
	 * 查询所有 附带子菜单
	 * @return
	 */
	List<SysMenu> selectAllWithChild();
	
	/**
	 * 查询所有
	 * @return
	 */
	List<SysMenu> selectAll();
	
}