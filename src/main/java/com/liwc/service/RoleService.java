package com.liwc.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.liwc.dto.RoleDto;
import com.liwc.message.ResponseMessage;
import com.liwc.model.SysMenuTree;
import com.liwc.model.SysRole;

import net.sf.json.JSONArray;


/**
 * 
 * 模块：基于RBAC的基础权限框架demo
 * 描述：
 * @author liwc
 * @date 2018年3月25日 下午10:02:55
 * @ RoleService
 */
public interface RoleService {

	
	/**
	 * 根据主键查询
	 * @param id
	 * @return
	 */
	public RoleDto findById(Integer id);
	
	/**
	 * 获取分页数据
	 * @param dto
	 * @return
	 */
	public PageInfo<SysRole> findAll(RoleDto dto);
	
	/**
	 * 获取全部根元素
	 * @return
	 */
	public List<SysRole> findAllWithChild();
	
	public List<SysMenuTree> selectTreeMenu();
	
	
	public ResponseMessage save(SysRole resource);
	
	ResponseMessage deleteById(Integer id);

	public List<SysRole> getRootList();

	public List<SysRole> getByExample(RoleDto dto);

	public ResponseMessage saveRoleMenu(Integer id, RoleDto dto, JSONArray menuArr);
	
	
	
}
