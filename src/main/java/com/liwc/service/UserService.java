package com.liwc.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.liwc.dto.UserDto;
import com.liwc.message.ResponseMessage;
import com.liwc.model.SysMenu;
import com.liwc.model.SysUser;
import com.liwc.model.page.PageEasyui;



/**
 * 
 * 标题：基于RBAC的基础权限框架demo
 * 功能：
 * 描述：
 * @author liwc
 * @date 2018年4月2日 上午11:48:15
 * @ UserService
 */
public interface UserService {

	
	public UserDto findById(Integer id);
	
	public PageInfo<SysUser> findAll(UserDto dto);
	
	public SysUser findUserByName(String userName);

	public List<SysUser> findUsers(Map<String, Object> params);
	
	public List<SysUser> getByExample(UserDto dto);
	
	public List<SysMenu> findResourcesByUserId(Integer userId);
	
	public Map<String, String> findResourceMap(Integer userId);
	
	
	
	public ResponseMessage save(SysUser resource);
	
	public ResponseMessage deleteById(Integer id);

	public List<SysUser> updateStatus(Integer[] ids, Integer status);
	
	
}
