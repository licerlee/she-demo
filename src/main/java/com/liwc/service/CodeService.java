package com.liwc.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.liwc.dto.CodeDto;
import com.liwc.message.ResponseMessage;
import com.liwc.model.SysCode;
import com.liwc.model.SysMenuTree;




public interface CodeService {

	
	/**
	 * 根据主键查询
	 * @param id
	 * @return
	 */
	public CodeDto findById(Integer id);
	
	/**
	 * 获取分页数据
	 * @param dto
	 * @return
	 */
	public PageInfo<SysCode> findAll(CodeDto dto);
	
	/**
	 * 获取全部根元素
	 * @return
	 */
	public List<SysCode> findAllWithChild();
	
	public List<SysMenuTree> selectTreeMenu();
	
	
	public ResponseMessage save(SysCode entity);
	
	ResponseMessage deleteById(Integer id);

	public List<SysCode> getRootList();

	public List<SysCode> getByExample(CodeDto dto);
	
	
	
}
