package com.liwc.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.liwc.dto.MenuDto;
import com.liwc.message.ResponseMessage;
import com.liwc.model.SysMenu;
import com.liwc.model.SysMenuTree;
import com.liwc.model.page.PageEasyui;




public interface MenuService //<T> extends BaseService<T>
{

	
	/**
	 * 根据主键查询
	 * @param id
	 * @return
	 */
	public MenuDto findById(Integer id);
	
	/**
	 * 获取分页数据
	 * @param dto
	 * @return
	 */
	public PageInfo<SysMenu> findAllMenu(MenuDto dto);
	
	/**
	 * 获取全部根元素
	 * @return
	 */
	public List<SysMenu> findAllWithChild();
	
	public List<SysMenuTree> selectTreeMenu(MenuDto dto);
	
	
	public ResponseMessage save(SysMenu resource);
	
	ResponseMessage deleteById(Integer id);
	
	
	
}
