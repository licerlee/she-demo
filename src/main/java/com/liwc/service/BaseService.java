package com.liwc.service;

import java.util.List;

import com.github.pagehelper.PageInfo;

public interface BaseService<T, C> {

	
	
	/**
	 * 主键查找
	 * @param id
	 * @return
	 */
	public T findById(String id);

	/**
	 * 查找所有
	 * @return
	 */
	public List<T> findAll();
	
	/**
	 * 分页查询
	 * @return
	 */
	public PageInfo<T> findPageAll();
	
	/**
	 * 根据条件查找
	 * @param c 条件
	 * @return
	 */
	public List<T> findByCondi(C c);
	
	/**
	 * 根据条件查找分页
	 * @param c 条件
	 * @return
	 */
	public PageInfo<T> findPageByCondi(C c);
	
	/**
	 * 新增一个
	 * @param t
	 * @return
	 */
	public T add(T t);
	
	/**
	 * 按主键更新
	 * @param t
	 * @return
	 */
	public T updateById(T t);
	
	/**
	 * 按主键删除
	 * @return
	 */
	public int deleteById(String id);
	
	

}
