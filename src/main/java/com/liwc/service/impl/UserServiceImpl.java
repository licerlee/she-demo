package com.liwc.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.liwc.constant.StatusCode;
import com.liwc.dao.SysUserMapper;
import com.liwc.dto.UserDto;
import com.liwc.message.ResponseMessage;
import com.liwc.model.SysMenu;
import com.liwc.model.SysUser;
import com.liwc.model.page.PageEasyui;
import com.liwc.service.UserService;
import com.liwc.util.Md5Util;


@Service
public class UserServiceImpl implements UserService{
	
	
	// log
	private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
		
	@Autowired
	private SysUserMapper dao;
	
	
	
	

	@Override
	public SysUser findUserByName(String userName) {
		
		List<SysUser> users = dao.findUserByName(userName);
		if(!users.isEmpty()) 
			return users.get(0);
		else 
			return null;
	}

	@Override
	public List<SysUser> findUsers(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SysUser> updateStatus(Integer[] ids, Integer status) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SysMenu> findResourcesByUserId(Integer userId) {
		return dao.findResourcesByUserId(userId);
	}

	@Override
	public Map<String, String> findResourceMap(Integer userId) {
		Map<String, String> map = new HashMap<String, String>();
		
		List<SysMenu> resources = findResourcesByUserId(userId);
		if(resources != null && !resources.isEmpty()){
			for(SysMenu r : resources){
				map.put(String.valueOf(r.getId()), r.getMenuName());
			}
		}
		
		return map;
	}


	@Override
	public UserDto findById(Integer id) {
		
		if(null != id){
			UserDto dto = new UserDto();
			BeanUtils.copyProperties(dao.selectByPrimaryKey(id), dto);
			return dto;
		}else{
			return new UserDto();
		}
		
	}

	@Override
	public PageInfo<SysUser> findAll(UserDto dto) {
		
		PageHelper.startPage(dto.getPage(), dto.getRows());
		List<SysUser> list = dao.selectAll(dto);
		PageInfo<SysUser> pi = new PageInfo<SysUser>(list);
		return pi;
	}

	@Override
	public List<SysUser> getByExample(UserDto dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseMessage save(SysUser resource) {
		if(null != resource.getId() ){
			dao.updateByPrimaryKeySelective(resource);
		}else{
			resource.setUserPasswd(Md5Util.generatePassword(resource.getUserPasswd()));
			dao.insertSelective(resource);
		}
		
		return new ResponseMessage(StatusCode.OK, "保存成功", resource);
	}

	@Override
	public ResponseMessage deleteById(Integer id) {
		dao.deleteByPrimaryKey(id);
		return new ResponseMessage(StatusCode.OK, "删除成功", id);
	}

	
	
	
}
