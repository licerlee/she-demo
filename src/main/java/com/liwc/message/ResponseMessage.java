package com.liwc.message;

import java.io.Serializable;

public class ResponseMessage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	
	private Integer statusCode;
	
	private String msg;
	
	private Object data;

	
	
	public ResponseMessage(Integer statusCode, String msg, Object data) {
		this.statusCode = statusCode;
		this.msg = msg;
		this.data = data;
	}
	
	
	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}





	
}
