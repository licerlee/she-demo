package com.liwc.model;

import java.util.Set;

import lombok.Data;

@Data
public class SysRole extends BaseModel {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String roleCode;

	private String roleName;

	private Integer roleOrder;

	private Integer roleStatus;

	private String roleDesc;

	private Set<SysMenu> menus;

	
}