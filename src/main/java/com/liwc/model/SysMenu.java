package com.liwc.model;

import java.util.List;
import java.util.Set;

import lombok.Data;


@Data
public class SysMenu extends BaseModel{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String menuName;

	private String menuUrl;

	private Integer menuOrder;

	private Integer menuParent;

	private Integer menuLevel;

	private String menuType;

	private String menuIcon;
	
	
	
	
	private transient String menuParentName;
	
	// 一对多关联字段
	private List<SysMenu> children;

}