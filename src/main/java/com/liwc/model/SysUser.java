package com.liwc.model;

import java.util.Set;

import lombok.Data;

@Data
public class SysUser extends BaseModel {

	public final static Integer STATUS_DEFAULT = 0; // 不可用

	public final static Integer STATUS_YES = 1; // 可用
	
	public final static Integer STATUS_NO = 2; // 不可用
	
	


	private String userLoginId;

	private String userPasswd;

	private String userName;

	private Integer userStatus;

	private Integer userType;

	private Integer userRole;

	private Set<SysRole> roles;

	private Set<SysMenu> menus;

	
	
	
	
	
//	public Set<SysRole> getRoles() {
//		return roles;
//	}
//
//	public void setRoles(Set<SysRole> roles) {
//		this.roles = roles;
//	}
//
//	public Set<SysMenu> getMenus() {
//		return menus;
//	}
//
//	public void setMenus(Set<SysMenu> menus) {
//		this.menus = menus;
//	}
//
//	public Integer getId() {
//		return id;
//	}
//
//	public void setId(Integer id) {
//		this.id = id;
//	}
//
//	public String getUserLoginId() {
//		return userLoginId;
//	}
//
//	public void setUserLoginId(String userLoginId) {
//		this.userLoginId = userLoginId == null ? null : userLoginId.trim();
//	}
//
//	public String getUserPasswd() {
//		return userPasswd;
//	}
//
//	public void setUserPasswd(String userPasswd) {
//		this.userPasswd = userPasswd == null ? null : userPasswd.trim();
//	}
//
//	public String getUserName() {
//		return userName;
//	}
//
//	public void setUserName(String userName) {
//		this.userName = userName == null ? null : userName.trim();
//	}
//
//	public Integer getUserStstus() {
//		return userStstus;
//	}
//
//	public void setUserStstus(Integer userStstus) {
//		this.userStstus = userStstus;
//	}
//
//	public Integer getUserType() {
//		return userType;
//	}
//
//	public void setUserType(Integer userType) {
//		this.userType = userType;
//	}
//
//	public Integer getUserRole() {
//		return userRole;
//	}
//
//	public void setUserRole(Integer userRole) {
//		this.userRole = userRole;
//	}
}