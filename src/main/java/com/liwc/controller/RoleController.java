package com.liwc.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.google.gson.Gson;
import com.liwc.dto.RoleDto;
import com.liwc.message.ResponseMessage;
import com.liwc.model.SysRole;
import com.liwc.model.page.EasyuiPageModel;
import com.liwc.service.RoleService;

import net.sf.json.JSONArray;

/**
 * 
 * 模块：基于RBAC的基础权限框架demo
 * 描述：角色管理
 * @author liwc
 * @date 2018年3月25日 下午10:01:00
 * @ RoleController
 */
@Controller
@RequestMapping(value="system/role")
public class RoleController {

	private static Logger log = LoggerFactory.getLogger(RoleController.class);
	
	private static final String PERFIX = "system/role";
	

	// 账户类型service
	@Autowired
	private RoleService service;
	
	
	/**
	 * 打开首页
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping(value="index")
	public String index(){
		
		return PERFIX+"/index";
	}
	
	/**
	 * 查询
	 * @param dto
	 * @return
	 */
	@RequestMapping(value = "list")
	@ResponseBody
	public EasyuiPageModel pageList(RoleDto dto) {

		// 分页查询
		PageInfo<SysRole> pi = service.findAll(dto);
		
		log.debug("----->总数量："+ pi.getTotal() +",分页数量："+ pi.getList().size());
		
		// 返回封装eui对象
		return new EasyuiPageModel(pi.getTotal(), pi.getList());
	}
	
	
	
	
	@RequestMapping(value = "info")
	public String info(Integer id, Model model) {
		
		RoleDto dto = service.findById(id);
		
		model.addAttribute("vo", dto);
		
		return PERFIX+"/info";
	}
	
	
	
	@RequestMapping(value = "save", method=RequestMethod.POST)
	@ResponseBody
	public ResponseMessage save(SysRole record) {
		return service.save(record);
	}
	
	
	
	@RequestMapping(value= "/delete", method=RequestMethod.POST)
	@ResponseBody
	public ResponseMessage delete(@RequestParam("id") Integer id){
		return service.deleteById(id);
	}
	
	
	@RequestMapping(value="authMenu")
	public String tree(){
		
		return PERFIX+"/authMenu";
		
	}
	
	@RequestMapping(value="saveRoleMenu")
	@ResponseBody
	public ResponseMessage saveRoleMenu(@RequestParam("id") Integer id, RoleDto dto, @RequestParam("menuTree") String menuTree){
		
		Gson gson = new Gson();
		JSONArray menuArr = JSONArray.fromObject(menuTree);
		
		return service.saveRoleMenu(id, dto, menuArr);
		
	}
	
	@RequestMapping(value = "all")
	@ResponseBody
	public List<SysRole> all(RoleDto dto) {

		return service.getByExample(dto);
	}
	
}
