package com.liwc.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.liwc.dto.CodeDto;
import com.liwc.message.ResponseMessage;
import com.liwc.model.SysCode;
import com.liwc.model.page.EasyuiPageModel;
import com.liwc.service.CodeService;

/**
 * 
 * 模块：基于RBAC的基础权限框架demo
 * 描述：
 * @author liwc
 * @date 2018年3月25日 下午1:44:54
 * @ CodeController
 */
@Controller
@RequestMapping(value="system/code")
public class CodeController {

	private static Logger log = LoggerFactory.getLogger(CodeController.class);
	
	private static final String PERFIX = "system/code";
	

	// 账户类型service
	@Autowired
	private CodeService service;
	
	
	/**
	 * 打开首页
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping(value="index")
	public String index(){
		
		return PERFIX+"/index";
	}
	
	
	/**
	 * 获取分页数据
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "list")
	@ResponseBody
	public EasyuiPageModel pageList(CodeDto dto) {

		// 分页查询
		PageInfo<SysCode> pi = service.findAll(dto);
		
		log.debug("----->总数量："+ pi.getTotal() +",分页数量："+ pi.getList().size());
		
		// 返回封装eui对象
		return new EasyuiPageModel(pi.getTotal(), pi.getList());
	}
	
	
	@RequestMapping(value = "info")
	public String info(Integer id, Model model) {
		
		CodeDto dto = service.findById(id);
		
		model.addAttribute("code", dto);
		
		return PERFIX+"/info";
	}
	
	
	@RequestMapping(value = "getRootList")
	@ResponseBody
	public List<SysCode> getRootList() {
		
		return service.getRootList();
	}
	
	
	@RequestMapping(value = "save", method=RequestMethod.POST)
	@ResponseBody
	public ResponseMessage save(SysCode record) {
		return service.save(record);
	}
	
	
	
	@RequestMapping(value= "/delete", method=RequestMethod.POST)
	@ResponseBody
	public ResponseMessage delete(@RequestParam("id") Integer id){
		return service.deleteById(id);
	}
	
	
	@RequestMapping(value="getByExample")
	@ResponseBody
	public List<SysCode> getByExample(CodeDto dto){
		
		List<SysCode> list = service.getByExample(dto);
		return list;
		
	}
}
