package com.liwc.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.liwc.dto.UserDto;
import com.liwc.message.ResponseMessage;
import com.liwc.model.SysUser;
import com.liwc.model.page.EasyuiPageModel;
import com.liwc.service.UserService;

/**
 * 
 * 标题：基于RBAC的基础权限框架demo
 * 功能：
 * 描述：
 * @author liwc
 * @date 2018年4月2日 下午3:09:35
 * @ UserController
 */
@Controller
@RequestMapping(value="system/user")
public class UserController {

	private static Logger log = LoggerFactory.getLogger(UserController.class);
	
	private static final String PERFIX = "system/user";
	

	// 账户类型service
	@Autowired
	private UserService service;
	
	
	/**
	 * 打开首页
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping(value="index")
	public String index(){
		
		return PERFIX+"/index";
	}
	
	/**
	 * 查询
	 * @param dto
	 * @return
	 */
	@RequestMapping(value = "list")
	@ResponseBody
	public EasyuiPageModel pageList(UserDto dto) {

		// 分页查询
		PageInfo<SysUser> pi = service.findAll(dto);
		
		log.debug("----->总数量："+ pi.getTotal() +",分页数量："+ pi.getList().size());
		
		return new EasyuiPageModel(pi.getTotal(), pi.getList());
	}
	
	
	
	
	@RequestMapping(value = "info")
	public String info(Integer id, Model model) {
		
		UserDto dto = service.findById(id);
		
		model.addAttribute("vo", dto);
		
		return PERFIX+"/info";
	}
	
	
	
	@RequestMapping(value = "save", method=RequestMethod.POST)
	@ResponseBody
	public ResponseMessage save(SysUser record) {
		return service.save(record);
	}
	
	
	
	@RequestMapping(value= "/delete", method=RequestMethod.POST)
	@ResponseBody
	public ResponseMessage delete(@RequestParam("id") Integer id){
		return service.deleteById(id);
	}
	
	
	
}
