package com.liwc.controller.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.liwc.dto.CodeDto;
import com.liwc.message.ResponseMessage;
import com.liwc.model.SysCode;
import com.liwc.model.page.EasyuiPageModel;
import com.liwc.service.CodeService;

/**
 * 
 * 标题：基于RBAC的基础权限框架demo
 * 功能：
 * 描述：
 * @author liwc
 * @date 2018年3月27日 下午2:29:12
 * @ CodeRestController
 */
@RequestMapping(value="api/system/code")
@RestController
public class CodeRestController {

	private static Logger log = LoggerFactory.getLogger(CodeRestController.class);
	

	// 账户类型service
	@Autowired
	private CodeService service;
	
	
	/**
	 * 获取分页数据
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "list")
	public EasyuiPageModel pageList(CodeDto dto) {

		// 分页查询
		PageInfo<SysCode> pi = service.findAll(dto);
		
		log.debug("----->总数量："+ pi.getTotal() +",分页数量："+ pi.getList().size());
		
		// 返回封装eui对象
		return new EasyuiPageModel(pi.getTotal(), pi.getList());
	}
	
	
	@RequestMapping(value = "info/{id}")
	public CodeDto info(@PathVariable("id")Integer id, Model model) {
		
		return service.findById(id);
	}
	
	
	@RequestMapping(value = "getRootList")
	public List<SysCode> getRootList() {
		return service.getRootList();
	}
	
	
	@RequestMapping(value = "save", method=RequestMethod.POST)
	public ResponseMessage save(SysCode record) {
		return service.save(record);
	}
	
	
	
	@RequestMapping(value= "/delete", method=RequestMethod.POST)
	public ResponseMessage delete(@RequestParam("id") Integer id){
		return service.deleteById(id);
	}
	
	
	@RequestMapping(value="getByExample")
	public List<SysCode> getByExample(CodeDto dto){
		return service.getByExample(dto);
		
	}
}
