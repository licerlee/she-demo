package com.liwc.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.liwc.dto.MenuDto;
import com.liwc.message.ResponseMessage;
import com.liwc.model.SysMenu;
import com.liwc.model.SysMenuTree;
import com.liwc.model.page.EasyuiPageModel;
import com.liwc.service.MenuService;
import com.liwc.util.PageUtil;

/**
 * 
 * <p>标题：基于RBAC的基础权限框架demo</p>
 * <p>功能：</p>
 * <p>描述：</p>
 * @author liwc
 * @date 2018年3月26日 下午5:53:24
 * @ MenuController
 */
@Controller
@RequestMapping(value="system/menu")
public class MenuController {

	private static Logger log = LoggerFactory.getLogger(MenuController.class);
	
	private static final String PERFIX = "system/menu";
	

	// 账户类型service
	@Autowired
	private MenuService service;
	
	
	/**
	 * 打开首页
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping(value="index")
	public String index(Model model){
		model.addAttribute("title", "系统菜单管理");
		return PERFIX+"/index";
	}
	
	
	@RequestMapping(value="indexDatagrid")
	public String indexDatagrid(){
		
		return PERFIX+"/indexDatagrid";
	}
	
	@RequestMapping(value = "list")
	@ResponseBody
	public EasyuiPageModel list(MenuDto dto) {

		// 执行业务逻辑层
		List<SysMenu> dtos = service.findAllWithChild();
		long total = dtos.size();

		// 封装数据
		EasyuiPageModel easyuiPageModel = PageUtil.convert(total, dtos);

		return easyuiPageModel;
	}
	
	
	/**
	 * 获取分页数据
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "list1")
	@ResponseBody
	public EasyuiPageModel pageList(MenuDto dto) {

		// 分页查询
		PageInfo<SysMenu> pi = service.findAllMenu(dto);
		
		// 返回封装eui对象
		return new EasyuiPageModel(pi.getTotal(), pi.getList());
	}
	
	
	
	
	
	
	
	@RequestMapping(value = "info")
	public String info(Integer id, Model model) {
		
		MenuDto dto = service.findById(id);
		
		model.addAttribute("menu", dto);
		
		return PERFIX+"/info";
	}
	
	
	@RequestMapping(value = "findTreeMenu")
	@ResponseBody
	public List<SysMenuTree> findTreeMenu(MenuDto dto,  Model model) {
		
		return service.selectTreeMenu(dto);
		
	}
	
	
	@RequestMapping(value = "save", method=RequestMethod.POST)
	@ResponseBody
	public ResponseMessage save(SysMenu record) {
		return service.save(record);
	}
	
	
	
	@RequestMapping(value= "/delete", method=RequestMethod.POST)
	@ResponseBody
	public ResponseMessage delete(@RequestParam("id") Integer id){
		return service.deleteById(id);
	}
	
	
	@RequestMapping(value="tree")
	public String tree(){
		
		return PERFIX+"/tree";
		
	}
	
	@RequestMapping(value="easyTable")
	public String easyTable(){
		
		return PERFIX+"/easyTable";
		
	}
	
}
