<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<%@ include file="/commons/meta.jsp"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>账户添加/编辑页面</title>
</head>

<body>

	<form id="user_info_form" name="user_info_form" method="post" class="easyui-form" data-options="novalidate:true ">
		<!-- 页面flag：add/edit -->
		<input type="hidden" name="user_page_flag" id="user_page_flag" />
		<!-- 账户id -->
		<input type="hidden" id="id" name="id" value="${vo.id }">

		<div id="account_add_div">
			<table width="100%" style="margin: 0 auto;" border="0"
				cellspacing="0" cellpadding="0">
				<tr>
					<td id="main_form" bgcolor="#FFFFFF" style="vertical-align: top;">
						<div style="background: url('<c:out value="${ctx}"/>/static/images/inputPageHeadBg.gif') repeat-x scroll 0% 0% transparent; padding: 10px;">
							<div id="ItemBlock_Title">
								&nbsp; 
								<img border="0" src="<c:out value="${ctx}"/>/static/images/item_point.gif" />
								&nbsp;基本信息 &nbsp;
							</div>
						</div>

						<table class="addtable_class" width="100%" border="0">
							
							<tr>
								<td align='right'>账号：</td>
								<td><input type="text" id="userLoginId" name="userLoginId" class="easyui-textbox" prompt="请输入账号" style="width: 200px;" 
									data-options="
										iconCls:'icon-user-16',
										required:true, 
										value:['${vo.userLoginId }'],
										missingMessage:'内容不能为空', 
										invalidMessage:'填写的内容不正确！'
									" /> 
									<span class="Prompt">*</span>
								</td>
							</tr>
							<tr>
								<td align='right'>密码：</td>
								<td><input type="password" id="userPasswd" name="userPasswd" class="easyui-passwordbox" prompt="Password" style="width: 200px;" 
									data-options="required:true
										
										" /> 
									<span class="Prompt">*</span>
								</td>
							</tr>
							
							<tr>
								<td align='right'>确认密码：</td>
								<td><input type="password" id="userPasswdAgain" name="userPasswdAgain" class="easyui-passwordbox" prompt="Password" style="width: 200px;" 
									validType="confirmPass['#userPasswd']" data-options="required:true" /> 
									<span class="Prompt">*</span>
								</td>
							</tr>
							
							<tr>
								<td align='right'>姓名：</td>
								<td><input type="text" id="userName" name="userName" class="easyui-textbox " value="${vo.userName }" style="width: 200px;" 
									data-options="required:true" /> 
									<span class="Prompt">*</span>
								</td>
							</tr>
							<tr>
								<td align='right'>状态：</td>
								<td>
									<input class="easyui-combobox" name="userStatus" id="userStatus"
										data-options="
										required:true,
					                    url:'${ctx }/system/code/getByExample?categ=UserStatus',
					                    method:'get', valueField:'code', textField:'value',
					                    value:['${vo.userStatus }'],
					                    panelHeight:'auto',
					                    editable:false,
										missingMessage:'内容不能为空', 
										invalidMessage:'填写的内容不正确！'
					                    ">
									<span class="Prompt">*</span>
								</td>
							</tr>
							
							<tr>
								<td align='right'>类型：</td>
								<td><input type="text" id="userType" name="userType" class="easyui-textbox " value="${vo.userType }" style="width: 200px;" 
									data-options="required:true" /> 
									<span class="Prompt">*</span>
								</td>
							</tr>
							
							<tr>
								<td align='right'>角色：</td>
								<td>
									<input class="easyui-combobox" name="userRole" id="userRole" style="width:200px;" 
										data-options="
										required:true,
					                    url:'${ctx }/system/role/all',
					                    method:'get', valueField:'id', textField:'roleName',
					                    value:['${vo.userRole }'],
					                    editable:false,
					                    multiple:false,
					                    panelHeight:'auto',
					                    labelPosition: 'top'
					                    ">
									<span class="Prompt">*</span>
								</td>
							</tr>
							
						</table>
					</td>
				</tr>
			</table>
		</div>
	</form>
	
	 <script type="text/javascript">
        $.extend($.fn.validatebox.defaults.rules, {
            confirmPass: {
                validator: function(value, param){
                    var pass = $(param[0]).passwordbox('getValue');
                    return value == pass;
                },
                message: '密码不匹配！'
            }
        })
    </script>
    
</body>

</html>