<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<%@ include file="/commons/meta.jsp"%>
<!DOCTYPE html>
<html>

<head>
<title>用户列表</title>
</head>

<body>
	<div class="easyui-layout" style="width: 100%; height: 100%;">

		<!--条件区域-->
		<div data-options="region:'west', title:'检索条件', collapsible:true, collapsed:true " style="width: 250px; height: 100%; padding: 5px;">
			
			<form action="index" id="user_search_form">
			
				<table style="width: 100%; border-collapse: collapse;" border="1">

					<tr>
						<td style="height: 40px; background-color: rgb(238, 238, 238);" align="right">角色编号</td>
						<td width="100px" height="10"><input type="text" id="condition_user_code_input"></td>
					</tr>
					<tr>
						<td style="height: 40px; background-color: rgb(238, 238, 238);" align="right">角色名称</td>
						<td height="10"> <input type="text" id="condition_user_name_input"></td>
					</tr>
					<tr>
						<td style="height: 50px;" colspan="2">
							<div id="condition_btn_div" style="text-align: center;">
								<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'"
									onclick="searchUser();return false;">检索</a>
								
								&nbsp;&nbsp;&nbsp;&nbsp; 
								
								<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-redo'"
									onclick="resetUser();return false;">重置</a>
							</div>
						</td>
					</tr>
				</table>
			</form>
		</div>

		<!--列表区域-->
		<div data-options="region:'center' " style="width: 100%; height: 100%;">
			<table id="user_list_table" class="easyui-datagrid" title="用户列表" style="width:100%;height:100%"
				data-options="collapsible:false, pagination:true, rownumbers:true, pageSize:20, pageList:[20, 50, 100] ">
				<thead>
					<tr>
						<th data-options="field:'id',width:50, align:'center', sortable:true, hidden:false ">id</th>
						<th data-options="field:'userLoginId',width:100, align:'left', sortable:true ">用户名</th>
						<th data-options="field:'userName',width:80, align:'left' ">姓名</th>
						<th data-options="field:'userStatus',width:100, align:'center' ">状态</th>
						<th data-options="field:'userType',width:150, align:'center' ">类型</th>
						<th data-options="field:'userRole',width:80, align:'center' " >角色</th>
						<th data-options="field:'action', width:80, align:'center' ">操作</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>

	<!-- js必须引入在body内才能被加载-->
	<script type="text/javascript" src="<c:out value="${ctx}"/>/static/js/system/user.js"></script>
</body>
</html>