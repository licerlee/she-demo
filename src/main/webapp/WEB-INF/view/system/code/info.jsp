<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<%@ include file="/commons/meta.jsp"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>添加/编辑页面</title>
</head>

<body>

	<form id="code_info_form" name="code_info_form" class="easyui-form" method="post" data-options="novalidate:true ">
		<!-- 页面flag：add/edit -->
		<input type="hidden" name="code_page_flag" id="code_page_flag" />
		<!-- id -->
		<input type="hidden" id="id" name="id" value="${code.id }">

		<div id="account_add_div">
			<table width="100%" style="margin: 0 auto;" border="0"
				cellspacing="0" cellpadding="0">
				<tr>
					<td id="main_form" bgcolor="#FFFFFF" style="vertical-align: top;">
						<div
							style="background: url('<c:out value="${ctx}"/>/static/images/inputPageHeadBg.gif') repeat-x scroll 0% 0% transparent; padding: 10px;">
							<div id="ItemBlock_Title">
								&nbsp; 
								<img border="0" src="<c:out value="${ctx}"/>/static/images/item_point.gif" />
								&nbsp;基本信息 &nbsp;
							</div>
						</div>

						<table class="addtable_class" width="100%" border="0">
							<tr>
								<td align='right' width="20%"><label for="mName">分类：</label></td>
								<td width="70%">
									<input class="easyui-combobox" name="categ" id="categ"
										data-options="
										required:true, 
					                    url:'${ctx }/system/code/getRootList',
					                    method:'get', valueField:'categ', textField:'categName',
					                    value:['${code.categ}' ],
					                    multiple:false,
					                    editable:false,
					                    panelHeight:'auto',
										missingMessage:'内容不能为空', 
										invalidMessage:'填写的内容不正确！',
					                    labelPosition: 'top'
					                    ">
									<span class="Prompt">*</span>
								</td>
							</tr>
							<tr>
								<td align='right'>编号：</td>
								<td>
									<input class="easyui-textbox" name="code" id="code" value="${code.code }" 
										data-options="
										required:true, 
										missingMessage:'内容不能为空', 
										invalidMessage:'填写的内容不正确！' ">
									
									<span class="Prompt">*</span>
								</td>
							</tr>
							
							<tr>
								<td align='right'>名称：</td>
								<td>
									<input class="easyui-textbox" name="value" id="value" value="${code.value }"
									data-options="
										required:true, 
										missingMessage:'内容不能为空', 
										invalidMessage:'填写的内容不正确！' ">
									<span class="Prompt">*</span></td>
							</tr>
							
						</table>
					</td>
				</tr>
			</table>
		</div>
	</form>
	
	<script type="text/javascript">

$(function(){

	
});


</script>

</body>

</html>