<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<%@ include file="/commons/meta.jsp"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>首页</title>
</head>

<body>
	<div class="easyui-layout" style="width: 100%; height: 100%;">

		<!--条件区域-->
		<div data-options="region:'west', title:'检索条件', collapsed:true " style="width: 250px; height: 100%; padding: 5px;">
			
			<form action="index" id="code_search_condition_form">
			
				<table style="width: 100%; border-collapse: collapse;" border="1">

					<tr>
						<td style="height: 40px; background-color: rgb(238, 238, 238);" align="right">代码项ID</td>
						<td width="100px" height="10"><input type="text" id="condition_code_id_input" name="id">
						</td>
					</tr>
					<tr>
						<td style="height: 40px; background-color: rgb(238, 238, 238);" align="right">代码项名称</td>
						<td height="10"><input type="text" id="condition_code_name_input" name="cCateg">
						</td>
					</tr>
					
					<tr>
						<td style="height: 50px;" colspan="2">
							<div id="condition_btn_div" style="text-align: center;">
								<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="searchCode();return false;">检索</a>
								&nbsp;&nbsp;&nbsp;&nbsp; 
								<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-redo'" onclick="resetcode();return false;">重置</a>
							</div></td>
					</tr>
					
				</table>
			</form>
		</div>

		<!--列表区域-->
		<div data-options="region:'center' "
			style="width: 70%; height: 100%;">
			
			<table id="code_list_table" class="easyui-datagrid" title="代码项列表" style="width:100%; height:100%" 
				data-options="collapsible:false, pagination: true, rownumbers:true, pageSize:20,pageList: [20, 50, 100] ">
				<thead>
					<tr>
						<th data-options="field:'id', width:20, align:'left', sortable:true, hidden:false ">ID</th>
						<th data-options="field:'code', width:100, align:'left' ">编号</th>
						<th data-options="field:'value', width:50, align:'left' ">值</th>
						<th data-options="field:'categ', width:50, align:'left' ">类型编号</th>
						<th data-options="field:'categName', width:50, align:'left' ">类型名称</th>
					</tr>
				</thead>
			</table>
			
		</div>
		
	</div>

	<!-- 页面私有资源 (根据业务需要引用) tips：js必须引入在body内才能被加载-->
	<script type="text/javascript"
		src="<c:out value="${ctx}"/>/static/js/system/code.js"></script>
</body>
</html>