<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<%@ include file="/commons/meta.jsp"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>menu tree</title>
</head>

<body>

	<form id="" name="" class="easyui-form" method="post" >

		<table id="easytable_menu_list_table" class="easyui-datagrid" title="" style="width:100%;height:100%"
			data-options="collapsible:false, pagination: true, rownumbers:true, pageSize:20,pageList: [20, 50, 100] ">
			<thead>
				<tr>
					<th data-options="field:'id',width:50, align:'center', sortable:true, hidden:false ">ID</th>
					<th data-options="field:'menuName',width:100, align:'left' ">名称</th>
					<th data-options="field:'menuType',width:50, align:'center' ">类型</th>
					<th data-options="field:'menuLevel',width:50, align:'center' ">等级</th>
					<th data-options="field:'menuIcon',width:50, align:'left' ">图标</th>
					<th data-options="field:'menuUrl',width:100, align:'left' ">链接</th>
					<th data-options="field:'menuParent',width:50, align:'left' ">父菜单</th>
					<th data-options="field:'menuOrder',width:20, align:'center', sortable:true ">排序</th>
				</tr>
			</thead>
		</table>
	</form>
	
	<script type="text/javascript">
		$(document).ready(function() {
			
			var $easytable_menu_list_table = $("#easytable_menu_list_table");
			loadEasyTableMenuList();
			
			function loadEasyTableMenuList() {
				$easytable_menu_list_table.datagrid({
					//title: "菜单信息",
					method: "post",
					url: _ctx+"/system/menu/list1",
					queryParams: $("#menu_search_condition_form :input").serializeArray(),
					fit: true,
					sortName: 'id',
					sortOrder: 'asc',
					rownumbers: true,
					pagination: true,
					striped: true,
					fitColumns: true,
					autoRowHeight: false,
					pageNumber: 1, //在设置分页属性的时候初始化页码
					showFooter: true,
					ctrlSelect: true,
					singleSelect: false,
					onDblClickRow: function(rowIndex, rowData) {
						openMenuPage('edit', rowData.id);
					},
					frozenColumns: [
						[{
							field: '',
							checkbox: true
						}]
					]
				});
			}
		});
		
		
	</script>

</body>

</html>