<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<%@ include file="/commons/meta.jsp"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>添加/编辑页面</title>
</head>

<body>

	<form id="menu_info_form" name="menu_info_form" class="easyui-form" method="post" data-options="novalidate:true "><!-- 是否关闭验证 -->
		<!-- 页面flag：add/edit -->
		<input type="hidden" name="menu_page_flag" id="menu_page_flag" />
		<!-- id -->
		<input type="hidden" id="id" name="id" value="${menu.id }">

		<div id="account_add_div">
			<table width="100%" style="margin: 0 auto;" border="0"
				cellspacing="0" cellpadding="0">
				<tr>
					<td id="main_form" bgcolor="#FFFFFF" style="vertical-align: top;">
						<div
							style="background: url('<c:out value="${ctx}"/>/static/images/inputPageHeadBg.gif') repeat-x scroll 0% 0% transparent; padding: 10px;">
							<div id="ItemBlock_Title">
								&nbsp; 
								<img border="0" src="<c:out value="${ctx}"/>/static/images/item_point.gif" />
								&nbsp;基本信息 &nbsp;
							</div>
						</div>

						<table class="addtable_class" width="100%" border="0">
							<tr>
								<td align='right' width="20%"><label for="menuName">名称：</label></td>
								<td width="70%">
									<input class="easyui-textbox" type="text" id="menuName" name="menuName" value="${menu.menuName }" style="width: 200px;" 
										data-options="
										required:true, 
										missingMessage:'菜单名称不能为空', 
										invalidMessage:'菜单名称填写的格式不正确！',
										validateOnBlur:true " /> 
									<span class="Prompt">*</span>
								</td>
							</tr>
							<tr>
								<td align='right'>图标：</td>
								<td>
									<input class="easyui-combobox" name="menuIcon" id="menuIcon"
										data-options="
										required:true,
										showItemIcon: true,
					                    url:'${ctx }/system/code/getByExample?categ=icon',
					                    method:'get', valueField:'code', textField:'value', 
					                    value:['${menu.menuIcon }'],
					                    panelHeight:'auto',
					                    editable:false,
										missingMessage:'图标内容不能为空', 
										invalidMessage:'填写的内容不正确！'
					                    ">
									<span class="Prompt">*</span>
								</td>
							</tr>
							
							<tr>
								<td align='right'>类型：</td>
								<td>
									<c:choose>
										<c:when test="${menu.menuType eq 'dir' }">
											<label>
												<input type="radio" name="menuType" value="dir" checked="checked">目录</input>
											</label>
											<label>
												<input type="radio" name="menuType" value="page" >页面</input>
											</label>
										</c:when>
										
										<c:when test="${menu.menuType eq 'page' }">
											<label>
												<input type="radio" name="menuType" value="dir">目录</input>
											</label>
											<label>
												<input type="radio" name="menuType" value="page" checked="checked">页面</input>
											</label>
										</c:when>
										<c:otherwise>
											<label>
												<input type="radio" name="menuType" value="dir">目录</input>
											</label>
											<label>
												<input type="radio" name="menuType" value="page" >页面</input>
											</label>
										</c:otherwise>
									</c:choose>
									<span class="Prompt">*</span></td>
							</tr>
							
							<c:choose>
								<c:when test="${menu.menuType eq 'dir' }">
									<tr id="parentMenuSelect" style="display:none ">
										<td align='right'>父菜单：</td>
										<td >
											<input type="text" class="easyui-textbox" name="menuParent" id="menuParent" style="width: 200px;" >
											<a id="menuParent-element" href="javascript:;;">
												<img style="vertical-align: middle;" alt="search" src="${ctx }/static/plugins/easyui/themes/icons/search.png">
											</a> 
									</tr>
									<tr id="urltr" style="display: none;">
										<td align='right'>URL：</td>
										<td><input type="text" class="easyui-textbox" name="menuUrl" id="menuUrl" value="${menu.menuUrl }" style="width: 300px;"/> 
											<span class="Prompt">*</span>
										</td>
									</tr>
								</c:when>
								
								<c:when test="${menu.menuType eq 'page' }">
									<tr id="parentMenuSelect" >
										<td align='right'>父菜单：</td>
										<td>
											<input type="text" class="easyui-textbox" name="menuParent" id="menuParent" value="${menu.menuParent }" style="width: 200px;" 
												>
											<a id="menuParent-element" href="javascript:;;" >
												<img style="vertical-align: middle;" alt="search" src="${ctx }/static/plugins/easyui/themes/icons/search.png">
											</a> 
									</tr>
									<tr id="urltr" >
										<td align='right'>URL：</td>
										<td><input type="text" class="easyui-textbox" id="menuUrl" name="menuUrl" value="${menu.menuUrl }" style="width: 300px;"/> 
											<span class="Prompt">*</span>
										</td>
									</tr>
								</c:when>
								
								<c:otherwise>
									<tr id="parentMenuSelect" >
										<td align='right'>父菜单：</td>
										<td>
											<input type="text" class="easyui-textbox" name="menuParent" id="menuParent" style="width: 200px;" 
												>
											<a id="menuParent-element" href="javascript:;;" ">
												<img style="vertical-align: middle;" alt="search" src="${ctx }/static/plugins/easyui/themes/icons/search.png">
											</a> 
									</tr>
									<tr id="urltr" >
										<td align='right'>URL：</td>
										<td><input type="text" class="easyui-textbox" id="menuUrl" name="menuUrl" value="${menu.menuUrl }" style="width: 300px;" /> 
											<span class="Prompt">*</span>
										</td>
									</tr>
								</c:otherwise>
							</c:choose>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</form>
	
	<script type="text/javascript">
$(function(){

	console.log("init mentType:"+ '${menu.menuType}' );
	// 初期化
	doParent("${menu.menuType}");
	
	// 类型 事件处理
	$('input[name="menuType"]').on('click', function(){
		var menuTypeValue = $(this).val();
		doParent(menuTypeValue);
	});
	
	// 父菜单事件处理
	$("#menuParent-element").on("click", function(){
		showDialog();
	});
	
	
	// 父菜单操作
	function doParent(menuTypeValue){
		if(menuTypeValue === 'page'){
			$('#parentMenuSelect').show();
			$('#urltr').show();
			
			$('#menuParent').validatebox({
				required: true,
				missingMessage: '名称不能为空 ',
				tipPosition: 'rigth'
			});
			
			/* $('#menuUrl').validatebox({
			    required: true,
			    validType: 'url'
			}); */
			
		}else if(menuTypeValue === 'dir'){
			$('#parentMenuSelect').hide();
			$('#urltr').hide();
			
			$('#menuParent').val("");
			$('#menuUrl').val("");
			
			$('#menuParent').validatebox({
			    required: false
			});
			
			/* $('#menuUrl').validatebox({
			    required: false
			}); */
			
		}else {
			$('#parentMenuSelect').hide();
			$('#urltr').hide();
			
		}
	}
	
	// $验证
	var validator = $("#menu_info_form").validate({
		rules: {
			menuType: "required"
		},
		messages: {
			menuType: "请至少选择一项"
		},
		// the errorPlacement has to take the table layout into account
		errorPlacement: function(error, element) {
			if (element.is(":radio"))
				error.appendTo(element.parent().next().next());
			else if (element.is(":checkbox"))
				error.appendTo(element.next());
			else
				error.appendTo(element.parent().next());
		},
		// specifying a submitHandler prevents the default submit, good for the demo
		submitHandler: function() {
			//alert("submitted!");
			console.log("submitted");
		},
		// set this class to error-labels to indicate valid fields
		success: function(label) {
			// set &nbsp; as text for IE
			label.html("&nbsp;").addClass("checked");
		},
		highlight: function(element, errorClass) {
			$(element).parent().next().find("." + errorClass).removeClass("checked");
		}
	});
	
	
});


</script>
</body>
</html>