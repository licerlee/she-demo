<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<%@ include file="/commons/meta.jsp"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>menu tree</title>
</head>

<body>

	<form id="menu_tree_form" name="menu_tree_form" class="easyui-form" method="post" >

		<div >
			<table width="100%" style="margin: 0 auto;" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td id="main_form" bgcolor="#FFFFFF" style="vertical-align: top;">
						<div style="background: url('<c:out value="${ctx}"/>/static/images/inputPageHeadBg.gif') repeat-x scroll 0% 0% transparent; padding: 10px;">
							<div id="ItemBlock_Title">
								&nbsp; 
								<img border="0" src="<c:out value="${ctx}"/>/static/images/item_point.gif" />
								&nbsp;基本信息 &nbsp;
							</div>
						</div>

						<table class="" width="100%" border="0">
							<tr>
								<td align='right' width="20%">系统菜单：</td>
								<td colspan="1">
									<ul id="treeDemo" class="ztree"></ul>
								</td>
								
								
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</form>
	
	<script type="text/javascript">
		var setting = { 
		    data: { 
		        simpleData: { 
		            enable: true 
		        } 
		    } 
		}; 
		 
		var zNodes = [{ 
		    name: "父节点1 - 展开", 
		    open: true, 
		    children: [{ 
		        name: "父节点11 - 折叠", 
		        children: [{ 
		            name: "叶子节点111" 
		        }, 
		        { 
		            name: "叶子节点112" 
		        }] 
		    }, 
		    { 
		        name: "父节点12 - 折叠", 
		        children: [{ 
		            name: "叶子节点121" 
		        }, 
		        { 
		            name: "叶子节点122" 
		        }] 
		    }, 
		    { 
		        name: "父节点13 - 没有子节点", 
		        isParent: true 
		    }] 
		}, 
		{ 
		    name: "父节点2 - 折叠", 
		    children: [{ 
		        name: "父节点21 - 展开", 
		        open: true, 
		        children: [{ 
		            name: "叶子节点211" 
		        }, 
		        { 
		            name: "叶子节点212" 
		        }] 
		    }, 
		    { 
		        name: "父节点22 - 折叠", 
		        children: [{ 
		            name: "叶子节点221" 
		        }, 
		        { 
		            name: "叶子节点222" 
		        }] 
		    }, 
		    { 
		        name: "父节点23 - 折叠", 
		        children: [{ 
		            name: "叶子节点231" 
		        }] 
		    }] 
		}, 
		{ 
		    name: "父节点3 - 没有子节点", 
		    isParent: true 
		} 
		];
		
		var zNodes1 =[ 
            { id:1, pId:0, name:"展开、折叠 自定义图标不同", open:true, iconOpen: _ctx +"/static/plugins/zTree_v3/css/img/diy/1_open.png", iconClose: _ctx+"/static/plugins/zTree_v3/css/img/diy/1_close.png"}, 
            { id:11, pId:1, name:"叶子节点1", icon:_ctx +"/static/plugins/zTree_v3/css/img/diy/2.png"}, 
            { id:2, pId:0, name:"展开、折叠 自定义图标相同", open:true, icon:_ctx +"/static/plugins/zTree_v3/css/img/diy/4.png"}, 
            { id:21, pId:2, name:"叶子节点1", icon:_ctx +"/static/plugins/zTree_v3/css/img/diy/6.png"}, 
            { id:3, pId:0, name:"不使用自定义图标", open:true }, 
            { id:31, pId:3, name:"叶子节点1"} 
        ]; 
		
		var setting1 = {
				data: { 
			        simpleData: { 
			            enable: true 
			        } 
			    },
			    check: {
			    	enable: true
			    },
			    async: { 
			        enable: true, 
			        url: _ctx+"/system/menu/findTreeMenu", 
			        autoParam: ["id", "name"], 
			        otherParam: { 
			            "menuLevel": "1" 
			        },
			        dataFilter: filter 
			    } 
			}; 
		
		function filter(treeId, parentNode, childNodes) { 
		    if (!childNodes) return null; 
		    for (var i = 0, 
		    l = childNodes.length; i < l; i++) { 
		        childNodes[i].name = childNodes[i].name.replace(/\.n/g, '.'); 
		    } 
		    return childNodes; 
		} 
		
		$(document).ready(function() { 
			$.fn.zTree.init($("#treeDemo"), setting1); 
		});
		
		
	</script>

</body>

</html>