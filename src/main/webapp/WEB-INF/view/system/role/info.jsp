<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<%@ include file="/commons/meta.jsp"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>角色添加/编辑页面</title>

<link href="<c:out value="${ctx}"/>/static/js/uploadify/jquery.uploadify.css">


</head>

<body>
	<form id="role_add_form" name="role_add_form" method="post" class="easyui-form" data-options="novalidate:true ">
		<!-- 页面flag：add/edit -->
		<input type="hidden" name="role_info_page_flag" id="role_info_page_flag" />
		<!-- 角色id -->
		<input type="hidden" id="id" name="id" value="${vo.id }">

		<div id="role_add_div">
			<table width="100%" style="margin: 0 auto;" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td id="main_form" bgcolor="#FFFFFF" style="vertical-align: top;">
						<div style="background: url('<c:out value="${ctx}"/>/static/images/inputPageHeadBg.gif') repeat-x scroll 0% 0% transparent; padding: 10px;">
							<div id="ItemBlock_Title">
								&nbsp; 
								<img border="0" src="<c:out value="${ctx}"/>/static/images/item_point.gif" />
								&nbsp;基本信息 &nbsp;
							</div>
						</div>

						<table class="addtable_class" width="100%" border="0">
							<tr>
								<td align='right'>角色编号：</td>
								<td><input type="text" id="roleCode" name="roleCode" class="easyui-textbox" value="${vo.roleCode }" style="width: 200px;" 
									data-options="
										required:true, 
										missingMessage:'内容不能为空', 
										invalidMessage:'填写的内容不正确！' "/> 
									<span class="Prompt">*</span></td>
							</tr>
							<tr>
								<td align='right'>角色名称：</td>
								<td><input type="text" id="roleName" name="roleName" class="easyui-textbox" value="${vo.roleName }" style="width: 200px;" 
									data-options="
										required:true, 
										missingMessage:'内容不能为空', 
										invalidMessage:'填写的内容不正确！' "/>
									<span class="Prompt">*</span>
								</td>
							</tr>
							
							<tr>
								<td align='right'>状态：</td>
								<td>
									<input class="easyui-combobox" name="roleStatus" id="roleStatus"
										data-options="
										required:true,
										showItemIcon: true,
					                    url:'${ctx }/system/code/getByExample?categ=RoleStatus',
					                    method:'get', valueField:'code', textField:'value', 
					                    value:['${vo.roleStatus }'],
					                    panelHeight:'auto',
					                    editable:false,
										missingMessage:'内容不能为空', 
										invalidMessage:'填写的内容不正确！'
					                    ">
					                    
									<span class="Prompt">*</span>
								</td>
							</tr>
							
							<tr>
								<td align='right'>描述：</td>
								<td><input type="text" id="roleDesc" name="roleDesc" class="easyui-textbox" data-option="required:true"
									value="${vo.roleDesc }" style="width: 200px;"  />
									<span class="Prompt">*</span>
								</td>
							</tr>
							
						</table>
					</td>
					
				</tr>
			</table>
		</div>
	</form>


	<script type="text/javascript">
	$(function() {
	});
	</script>
	
</body>

</html>