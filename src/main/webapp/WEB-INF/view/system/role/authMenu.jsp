<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<%@ include file="/commons/meta.jsp"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>添加/编辑页面</title>
</head>

<body>

	<form id="menu_info_form" name="menu_info_form" class="easyui-form" method="post" >
		<!-- id -->
		<input type="hidden" id="id" name="id" value="">

		<div id="account_add_div">
			<table width="100%" style="margin: 0 auto;" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td id="main_form" bgcolor="#FFFFFF" style="vertical-align: top;">
						<div
							style="background: url('<c:out value="${ctx}"/>/static/images/inputPageHeadBg.gif') repeat-x scroll 0% 0% transparent; padding: 10px;">
							<div id="ItemBlock_Title">
								&nbsp; 
								<img border="0" src="<c:out value="${ctx}"/>/static/images/item_point.gif" />
								&nbsp;基本信息 &nbsp;
							</div>
						</div>

						<table class="" width="100%" border="1"  >
							<tr>
								<td align="center" colspan="1">所有菜单</td>
								<td align="center" colspan="1">授权菜单</td>
								
							</tr>
							<tr>
								<td colspan="1" width="50%">
									<ul id="authRoleMenuTreeDemo" class="ztree"></ul>
								</td>
								<td colspan="1" width="50%">
									<!-- <ul id="authRoleMenuTreeDemo1" class="ztree"></ul> -->
								</td>
								
								
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</form>
	
	<script type="text/javascript">
		
		var setting1 = {
				data: { 
			        simpleData: { 
			            //如果设置为 true，请务必设置 setting.data.simpleData 内的其他参数: 
			            // idKey / pIdKey / rootPId，并且让数据满足父子关系。
			        	enable: true,
			        	idKey: "id",
			        	pIdKey: "parentId",
			        	rootPId: ""
			        },
			        key: {
			        	checked: "isMenuChecked"
			        }
			    },
			    
			    check: {
			    	enable: true
			    },
			    
			    edit: {
			    	enable: true,
			    	showRemoveBtn: false,
					showRenameBtn: false
			    },
			    
			    callback: {
					beforeDrag: beforeDrag,
					beforeDrop: beforeDrop
				},

			    async: { 
			        enable: true, 
			        url: _ctx+"/system/menu/findTreeMenu", 
			        autoParam: ["id", "name"], 
			        otherParam: { 
			            "menuLevel": "2",
			            "roleId": $("#id").val()
			        },
			        dataFilter: filter 
			    } 
		}; 
		
		function filter(treeId, parentNode, childNodes) { 
		    if (!childNodes) return null; 
		    for (var i=0, l=childNodes.length; i<l; i++) { 
		        childNodes[i].name = childNodes[i].name.replace(/\.n/g, '.');
		        
		    } 
		    return childNodes; 
		} 
		
		function beforeDrag(treeId, treeNodes) {
			for (var i=0,l=treeNodes.length; i<l; i++) {
				if (treeNodes[i].drag === false) {
					return false;
				}
			}
			return true;
		}
		function beforeDrop(treeId, treeNodes, targetNode, moveType) {
			return targetNode ? targetNode.drop !== false : true;
		}

		
		$(document).ready(function() { 
		    $.fn.zTree.init($("#authRoleMenuTreeDemo"), setting1);
		    
			//$.fn.zTree.init($("#authRoleMenuTreeDemo1"), setting1);
		});
		
		
	</script>

</body>

</html>