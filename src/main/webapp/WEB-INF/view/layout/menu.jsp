<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<%@ include file="/commons/meta.jsp"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>菜单</title>
</head>


<!-- body 以外区域不加载 -->
<body>

<style type="text/css">

#menu_ul a {
text-decoration: none;
}

#menu_ul a:link {
 text-decoration: none;
}
#menu_ul a:visited {
 text-decoration: none;
}
#menu_ul a:hover {
 text-decoration: none;
}
#menu_ul a:active {
 text-decoration: none;
}

</style>

	<div id="menu_div" class="easyui-accordion" style="width:100%; height:99%;">
		<c:forEach items="${sessionScope.session_menu_resource }" var="r" >
			<div title="${r.menuName }" data-options="iconCls:'${r.menuIcon }', selected:false " style="overflow:auto;padding:10px;">
				<c:if test="${fn:length(r.children) > 0 }">
					<ul id="menu_ul" class="easyui-tree" data-options="animate:true" >
						<c:forEach items="${r.children }" var="child">
							<li data-options="iconCls:'icon-profile-16'">
							<a href="#" onclick="addTab('center_tabs_div','${child.menuName }', 
								'${ctx}'+'${child.menuUrl }')">
									${child.menuName }</a>
							</li>
						</c:forEach>
					</ul>
				</c:if>
			</div>
		</c:forEach>
	</div>
</body>
</html>