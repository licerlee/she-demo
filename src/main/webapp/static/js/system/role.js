/**
 * 角色管理js
 */

/**
 * list_table 必须放在最上面,否则出错
 */
var $role_list_table = $("#role_list_table");

$(document).ready(function() {

	loadRoleList();
});

/**
 * 加载列表数据
 */
function loadRoleList() {

	
	$role_list_table.datagrid({
		title : "角色信息",
		method : "post",
		url : _ctx + "/system/role/list",
		queryParams : $("#role_search_condition_form :input").serializeArray(),
		fit : true,
		sortName : 'id',//排序字段(传递后台)
		sortOrder : 'asc',//排序方式(传递后台)
		rownumbers : true,
		pagination : true,
		striped : true,
		fitColumns : true,
		autoRowHeight : false,
		loadMsg : '加载中, 请稍后...',
		pageNumber: 1, //在设置分页属性的时候初始化页码
		showFooter : true,
		ctrlSelect : false,//ctrl多选
		singleSelect : true,//是否单选
		rowStyler : function(index, row) {
		},
		onClickRow: function(index, row){
			
			//$("#role_layout_div").layout("expand", "east");
		},
		onDblClickRow : function(index, row) {
			//row的双击事件
			openRolePage('edit', row.id);
		},
		frozenColumns : [ [ {
			//冻结列
			field : '',
			checkbox : true
		} ] ],
		toolbar : [ {
			text : '添加',
			iconCls : 'icon-add',
			handler : function() {

				openRolePage('add', '');
			}
		}, '-', {
			text : '修改',
			iconCls : 'icon-edit',
			handler : function() {

				var row = $role_list_table.datagrid("getSelected");
				if (null == row) {
					$.messager.alert("错误", "请选择要修改的数据！");
					return false;
				}
				openRolePage('edit', row.id);
			}
		}, '-', {
			text : '删除',
			iconCls : 'icon-remove',
			handler : function() {

				var row = $role_list_table.datagrid("getSelected");
				if (null == row) {
					$.messager.alert("错误", "请选择要刪除的数据！");
					return false;
				}
				delRole(row);
			}

		}, '-', {
			text : '打印',
			iconCls : 'icon-print',
			handler : function() {

				$.messager.alert("消息", " do print");
			}
		}, '-', {
			text : '分配菜单',
			iconCls : 'icon-settings',
			handler : function() {

				var row = $role_list_table.datagrid("getSelected");
				if (null == row) {
					$.messager.alert("错误", "请选择要操作的数据！");
					return false;
				}
				
				authMenu('edit', row.id);
			}
		} ]
	});
}


/**
 * 打开角色页面
 * 
 * @param {Object}
 *            flag
 * @param {Object}
 *            id
 */
function openRolePage(flag, id) {

	var title = "";
	var href = "";
	if ("add" == flag) {
		title = "新增";
		href = _ctx + "/system/role/info";
	} else if ("edit" == flag) {
		title = "修改";
		href = _ctx + "/system/role/info?id=" + id;
	} else {
		$.messager.alert("错误", "状态错误！");
		return false;
	}

	$("<div id='role_popup_div'></div>").dialog({
		title : "角色【" + title + "】",
		maximizable : true,
		resizable : true,
		width : 400,
		height : 300,
		closed : false,
		cache : false,
		href : href,
		modal : true,
		onLoad : function() {
			$("#role_info_page_flag").val(flag);
		},
		onClose: function(){
			$("#role_popup_div").remove();
		},
		buttons : [ {
			text : "保存",
			iconCls : "icon-save",
			handler : function() {

				saveRole();
			}
		}, {
			text : "取消",
			iconCls : "icon-cancel",
			handler : function() {

				$.messager.confirm('确认', '还未提交保存,真的要关闭?', function(r) {
					if (r) {
						$("#role_popup_div").dialog("close");
					}
				});
			}
		} ]
	});
}

/**
 * 保存角色
 */
function saveRole() {

	$('#role_add_form').form({
		type : "POST",
		ajax : true,
		url : _ctx + "/system/role/save",
		onSubmit : function() {
			
			var bool = $(this).form('enableValidation').form('validate');
			console.log(bool);
			return bool;
		},
		success : function(data) {

			var data = $.parseJSON(data);
			$.messager.alert('消息', data.msg);
			
			$("#role_popup_div").dialog("close");
			roleListFlush();
			
		}
	});
	$('#role_add_form').submit();
}


/**
 * 检索按钮
 */
function searchRole() {
	
	roleListFlush();
	
	//$.messager.alert('我的消息', '频繁检索将增加服务器负担！', 'warning', flushRoleListCallback);
}

/**
 * 检索回调
 */
function flushRoleListCallback() {

	$.messager.progress();
	roleListFlush();
	$.messager.progress('close');

}

/**
 * 重置按钮
 */
function ResetRole() {

	$("#condition_role_code_input").val('');
	$("#condition_role_name_input").val('');

}

/**
 * 刷新列表
 */
function roleListFlush() {
	
	$role_list_table.datagrid("load", $("#role_search_condition_form :input").serializeArray() );
}


/**
 * 删除角色
 * 
 * @param {Object}
 *            id
 */
function delRole(row) {

	$.messager.confirm('确认', '您确认想要删除记录吗？', function(r) {
		if (r) {
			delRoleById(row);
		}
	});
}

/**
 * 根据id删除
 * 
 * @param id
 */
function delRoleById(row) {

	$.ajax({
		url : _ctx + "/system/role/delete",
		type : "post",
		dataType : "JSON",
		data:{"id": row.id},
//		data : row,
		success : function(backdata) {
//			var data = eval(backdata);
			$.messager.alert('消息', backdata.msg);
			roleListFlush();
			
		},
		error : function(xhr, msg, event) {
			alert(msg);
		}
	});

}

/**
 * 授权
 * @param id
 * @returns
 */
function authMenu(flag, id){
	
	var href = _ctx + "/system/role/authMenu?id=" + id;

	$("<div id='role_auth_popup_div'></div>").dialog({
		title : "分配权限",
		maximizable : false,
		resizable : true,
		width : 600,
		height : 500,
		closed : false,
		cache : false,
		href : href,
		modal : true,
		onLoad : function() {
			$("#id").val(id);
		},
		onClose: function(){
			$("#role_auth_popup_div").remove();
		},
		buttons : [ {
			text : "保存",
			iconCls : "icon-save",
			handler : function() {

				// 获取选中记录
				var treeObj = $.fn.zTree.getZTreeObj("authRoleMenuTreeDemo");
				var nodes = treeObj.getCheckedNodes();
				console.log(nodes);
				console.log(JSON.stringify(nodes));
				
				if(nodes.length === 0){
					$.messager.alert("error", "请选择一个目录");
					return;
				}else{
					console.log( "已选择："+ nodes.length);
					var menuTreeArr = [];
					for(var i=0; i<nodes.length; i++){
						var menuTree = {};
						var menuTrees = [];
						
						
						menuTree.menuTrees = menuTrees['id'] = nodes[i].id;
						
						menuTree.id = nodes[i].id;
						menuTree.name = nodes[i].name;
						menuTree.parentId = nodes[i].parentId;
						
						menuTreeArr.push(menuTree);
						
					}
					console.log(JSON.stringify(menuTreeArr));
					saveRoleMenu(id, nodes);
				}
				
				$("#role_auth_popup_div").dialog("close");
				
			}
		}, {
			text : "取消",
			iconCls : "icon-cancel",
			handler : function() {
				$.messager.confirm('确认', '还未提交保存,真的要关闭?', function(r) {
					if (r) {
						$("#role_auth_popup_div").dialog("close");
					}
				});
			}
		} ]
	});
	
}


function saveRoleMenu(id, nodes){
	
	$.ajax({
		url : _ctx + "/system/role/saveRoleMenu?id="+id,
		type : "post",
		dataType : "JSON",
		data: {
			"menuTree": JSON.stringify(nodes)
		},
		success : function(backdata) {
			$.messager.alert('消息', backdata.msg);
			roleListFlush();
			
		},
		error : function(xhr, msg, event) {
			alert("系统发生错误，请联系管理员！");
		}
	});
}

