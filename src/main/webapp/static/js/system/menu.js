/**
 * 菜单管理js
 */

/**
 * list_table 必须放在最上面,否则出错
 */
var $menu_list_table = $("#menu_list_table");

$(document).ready(function() {

	loadMenuList();

});





/**
 * 加载列表数据
 */
function loadMenuList() {

	$menu_list_table.treegrid({
		title : "菜单信息",
		method : "post",
		url : _ctx + "/system/menu/list",
		idField : 'id', //设置层级treeGrid
		treeField : 'menuName', // 设置层级treeGrid
		queryParams : $("#menu_search_condition_form :input").serializeArray(),
		fit : true,
		sortName : 'id',
		sortOrder : 'asc',
		rownumbers : true,
		pagination : false,
		striped : true,
		fitColumns : true,
		autoRowHeight : false,
		loadMsg : 'loading, please wait a moment',
		pageNumber : 1, // 在设置分页属性的时候初始化页码
		showFooter : true,
		ctrlSelect : true,
		singleSelect : true,
		rowStyler : function(index, row) {
			/*
			 * if (row.menuStatus == 'M') { return
			 * 'background-color:red;color:#fff;'; }
			 */
		},
		loadFilter : function(data) {
			// 返回过滤数据显示。该函数带一个参数'data'用来指向源数据（即：获取的数据源，比如Json对象）。您可以改变源数据的标准数据格式。这个函数必须返回包含'total'和'rows'属性的标准数据对象。
			if (data.menuStatus) {
				return data.menuStatus;
			} else {
				return data;
			}
		},
		onClickRow : function(row) {
			// alert(JSON.stringify(row));
		},
		onDblClickRow : function(row) {
			openMenuPage('edit', row.id);
		},
		frozenColumns : [ [ {
			field : '',
			checkbox : true
		} ] ],
		toolbar : [ {
			text : '添加',
			iconCls : 'icon-add',
			handler : function() {

				openMenuPage('add', '');
			}
		}, '-', {
			text : '修改',
			iconCls : 'icon-edit',
			handler : function() {

				var row = $menu_list_table.treegrid("getSelected");
				if (null == row) {
					alert("请选择要修改的数据！");
					return false;
				}
				openMenuPage('edit', row.id);
			}
		}, '-', {
			text : '删除',
			iconCls : 'icon-remove',
			handler : function() {
				var row = $menu_list_table.treegrid("getSelected");
				if (null == row) {
					alert("请选择要刪除的数据！");
					return false;
				}
				$.messager.confirm("警告", "确认要删除所选记录吗？", function(r) {
					if (r) {
						delMenu(row.id);
					}
				});

			}

		}, '-', {
			text : '全部折叠',
			iconCls : 'icon-remove',
			handler : function() {

				$menu_list_table.treegrid('collapseAll');
			}

		}, '-', {
			text : '全部展开',
			iconCls : 'icon-remove',
			handler : function() {

				$menu_list_table.treegrid('expandAll');
			}

		}, '-', {
			text : '打印',
			iconCls : 'icon-print',
			handler : function() {

				alert(" do print");
			}
		}, '-', {
			text : '快速刷新',
			iconCls : 'icon-reload',
			handler : function() {
				menuListFlush();
			}
		} ]
	});
}

/**
 * 删除菜单
 * 
 * @param {Object}
 *            id
 */
function delMenu(id) {

	$.ajax({
		type : "post",
		url : _ctx + "/system/menu/delete",
		dataType : "json",
		data : {
			"id" : id
		},
		success : function(backdata) {
			$.messager.alert("提示", backdata.msg);
			menuListFlush();

		},
		error : function(xhr, msg, event) {
			$.messager.alert("提示", msg);
		}

	});
}

/**
 * 打开菜单页面
 * 
 * @param {Object}
 *            flag
 * @param {Object}
 *            id
 */
function openMenuPage(flag, id) {

	var title = "";
	var href = "";
	if ("add" == flag) {
		title = "新增";
		href = _ctx + "/system/menu/info?id=";
	} else if ("edit" == flag) {
		title = "修改";
		href = _ctx + "/system/menu/info?id=" + id;
	} else {
		alert("非法操作！");
		return false;
	}

	$("<div id='menu_popup_div'></div>").dialog({
		title : "菜单【" + title + "】",
		maximizable : true,
		resizable : true,
		width : 600,
		height : 400,
		closed : false,
		cache : false,
		href : href,
		modal : true,
		onLoad : function() {
			$("#menu_page_flag").val(flag);
		},
		onClose: function(){
			$("#menu_popup_div").remove();
		},
		buttons : [ {
			text : "保存",
			iconCls : "icon-save",
			handler : function() {
				saveMenu();
			}
		}, {
			text : "取消",
			iconCls : "icon-cancel",
			handler : function() {
				var result = window.confirm("还未提交保存,真的要关闭?");
				if (result) {
					$("#menu_popup_div").dialog("close");
				}
			}
		} ]
	});
}

/**
 * 保存
 */
function saveMenu() {

	$('#menu_info_form').form({
		type : "POST",
		ajax : true,
		url : _ctx + "/system/menu/save",
		async : false,
		onSubmit : function() {
			var bool = $(this).form('enableValidation').form('validate');
			console.log(bool);
			return bool;
		},
		success : function(bd) {
			var data = $.parseJSON(bd);
			$.messager.alert('消息', data.msg);
			$("#menu_popup_div").dialog("close");
			menuListFlush();
		}
	});
	$('#menu_info_form').submit();
}

/**
 * 检索按钮
 */
function searchM() {
	menuListFlush();
}

/**
 * 重置按钮
 */
function ResetMenu() {

	$("#search_menu_id_input").val('');
	$("#search_menu_name_input").val('');
}

function menuListFlush() {
	$menu_list_table.treegrid("load", {
		"menuId" : $("#search_menu_id_input").val(),
		"menuName" : $("#search_menu_name_input").val()
	});
}

function getParent(value, row, index) {
	// alert(value);
	// if(row){
	// return row.parent.name;
	// }else{
	// return '';
	// }

}

function showDialog() {

	$("<div id='menu_tree_popup_div'></div>").dialog({
		title : "树形菜单",
		maximizable : true,
		resizable : true,
		width : 400,
		height : 400,
		closed : false,
		cache : false,
		href : _ctx + "/system/menu/tree",
		modal : true,
		onLoad : function() {
		},
		onClose: function(){
			// 务必移除
			$("#menu_tree_popup_div").remove();
		},
		buttons : [ {
			text : "确认",
			iconCls : "icon-save",
			handler : function() {
				
				// 获取选中记录
				var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
				var nodes = treeObj.getCheckedNodes();
				console.log(nodes);
				if(nodes.length != 1){
					$.messager.alert("error", "请选择一个菜单项！");
					return;
				}else{
					
					console.log( "已选择："+ nodes.length);
					$("#menuParent").textbox('setValue', nodes[0].id);
					$("#menuParent").textbox('setText', nodes[0].name);
					
					console.log(nodes[0].id);
					console.log(nodes[0].name);
				}
				
				$("#menu_tree_popup_div").dialog("close");
			}
		}, {
			text : "关闭",
			iconCls : "icon-cancel",
			handler : function() {
				$("#menu_tree_popup_div").dialog("close");
			}
		} ]
	});
	
}
