/**
 * 代码项管理js
 */

/**
 * list_table 必须放在最上面,否则出错
 */
var $code_list_table = $("#code_list_table");

$(document).ready(function() {

	loadCodeList();

});




/**
 * 加载列表数据
 */
function loadCodeList() {

	$code_list_table.datagrid({
		
		title : "代码项信息",
		method : "post",
		url : _ctx + "/system/code/list",
		queryParams : $("#code_search_condition_form :input").serializeArray(),
		fit : true,
		sortName : 'id',
		sortOrder : 'asc',
		rownumbers : true,
		pagination : true,
		striped : true,
		fitColumns : true,
		autoRowHeight : false,
		loadMsg : 'loading, please wait a moment',
		pageNumber : 1, // 在设置分页属性的时候初始化页码
		showFooter : true,
		ctrlSelect : true,
		singleSelect : true,
		rowStyler : function(index, row) {
		},
		onClickRow : function(index, row) {
			// alert(JSON.stringify(row));
		},
		onDblClickRow : function(index, row) {
			openCodePage('edit', row.id);
		},
		frozenColumns : [ [ {
			field : '',
			checkbox : true
		} ] ],
		toolbar : [ {
			text : '添加',
			iconCls : 'icon-add',
			handler : function() {

				openCodePage('add', '');
			}
		}, '-', {
			text : '修改',
			iconCls : 'icon-edit',
			handler : function() {

				var row = $code_list_table.datagrid("getSelected");
				if (null == row) {
					alert("请选择要修改的数据！");
					return false;
				}
				openCodePage('edit', row.id);
			}
		}, '-', {
			text : '删除',
			iconCls : 'icon-remove',
			handler : function() {

				var row = $code_list_table.datagrid("getSelected");
				if (null == row) {
					alert("请选择要刪除的数据！");
					return false;
				}

				$.messager.confirm("警告", "确认要删除所选记录吗？", function(r) {
					if (r) {
						delCode(row.id);
					}
				})

			}

		}, '-', {
			text : '打印',
			iconCls : 'icon-print',
			handler : function() {

				alert(" do print");
			}
		}, '-', {
			text : '快速刷新',
			iconCls : 'icon-reload',
			handler : function() {
				codeListFlush();
			}
		} ]
		
		
		
	});
}

/**
 * 删除代码项
 * 
 * @param {Object}
 *            id
 */
function delCode(id) {

	$.ajax({
		type : "post",
		url : _ctx + "/system/code/delete",
		dataType : "json",
		data : {
			"id" : id
		},
		success : function(backdata) {
			$.messager.alert("提示", backdata.msg);
			codeListFlush();

		},
		error : function(xhr, msg, event) {
			alert(msg);
		}

	});
}

/**
 * 打开代码项页面
 * 
 * @param {Object}
 *            flag
 * @param {Object}
 *            id
 */
function openCodePage(flag, id) {

	var title = "";
	var href = "";
	if ("add" == flag) {
		title = "新增";
		href = _ctx + "/system/code/info?id=";
	} else if ("edit" == flag) {
		title = "修改";
		href = _ctx + "/system/code/info?id=" + id;
	} else {
		alert("非法操作！");
		return false;
	}

	$("<div id='code_popup_div'></div>").dialog({
		title : "代码项【" + title + "】",
		maximizable : true,
		resizable : true,
		width : 600,
		height : 400,
		closed : false,
		cache : false,
		href : href,
		modal : true,
		onLoad : function() {
			$("#code_page_flag").val(flag);
		},
		onClose: function(){
			$("#code_popup_div").remove();
		},
		buttons : [ {
			text : "保存",
			iconCls : "icon-save",
			handler : function() {
				saveCode();
			}
		}, {
			text : "取消",
			iconCls : "icon-cancel",
			handler : function() {

				var result = window.confirm("还未提交保存,真的要关闭?");
				if (result) {
					$("#code_popup_div").dialog("close");
				}
			}
		} ]
	});
}

/**
 * 保存
 */
function saveCode() {

	$('#code_info_form').form({
		type : "POST",
		ajax : true,
		url : _ctx + "/system/code/save",
		async : false,
		onSubmit : function() {

			var bool = $(this).form('enableValidation').form('validate');
			console.log(bool);
			return bool;
		},
		success : function(bd) {
			var data = $.parseJSON(bd);
			$.messager.alert('消息', data.msg);
			$("#code_popup_div").dialog("close");
			codeListFlush();

		}
	});
	$('#code_info_form').submit();
}

/**
 * 检索按钮
 */
function searchCode() {
	codeListFlush();
}

/**
 * 重置按钮
 */
function resetCode() {

	$("#condition_code_id_input").val('');
	$("#condition_code_name_input").val('');
}

function codeListFlush() {
	$code_list_table.datagrid("load", {
//		"id" : $("#condition_code_id_input").val(),
//		"mName" : $("#condition_code_name_input").val()
	});
}


function showDialog() {

}
