/**
 * 菜单管理js
 */

/**
 * list_table 必须放在最上面,否则出错
 */
var $menu_list_table = $("#menu_list_table");

$(document).ready(function() {

	loadMenuList();
});

/**
 * 加载列表数据
 */
function loadMenuList() {

	$menu_list_table.datagrid({
		title: "菜单信息",
		method: "post",
		url: _ctx+"/system/menu/list1",
		queryParams: $("#menu_search_condition_form :input").serializeArray(),
		fit: true,
		sortName: 'id',
		sortOrder: 'asc',
		rownumbers: true,
		pagination: true,
		striped: true,
		fitColumns: true,
		autoRowHeight: false,
		pageNumber: 1, //在设置分页属性的时候初始化页码
		showFooter: true,
		ctrlSelect: true,
		singleSelect: false,
		rowStyler: function(index, row) {
			//if (row.menuType == 'dir') {
			//	return 'background-color:#a4c2f4;';
			//}
		},
		onDblClickRow: function(rowIndex, rowData) {
			openMenuPage('edit', rowData.id);
		},
		frozenColumns: [
			[{
				field: '',
				checkbox: true
			}]
		],
		toolbar: [{
			text: '添加',
			iconCls: 'icon-add',
			handler: function() {

				openMenuPage('add', '');
			}
		}, '-', {
			text: '修改',
			iconCls: 'icon-edit',
			handler: function() {
				var row = $menu_list_table.datagrid("getSelected");
				if (null == row) {
					alert("请选择要修改的数据！");
					return false;
				}
				openMenuPage('edit', row.id);
			}
		}, '-', {
			text: '删除',
			iconCls: 'icon-remove',
			handler: function() {
				var row = $menu_list_table.datagrid("getSelected");
				if (null == row) {
					alert("请选择要刪除的数据！");
					return false;
				}
				$.messager.confirm("警告", "确认要删除所选记录吗？", function(r) {
					if (r) {
						delMenu(row.id);
					}
				});
				
			}

		}, '-', {
			text: '打印',
			iconCls: 'icon-print',
			handler: function() {

				alert(" do print");
			}
		}]
	});
}

/**
 * 删除菜单
 * @param {Object} id
 */
function delMenu(id) {
	$.ajax({
		type : "post",
		url : _ctx + "/system/menu/delete",
		dataType : "json",
		data : {
			"id" : id
		},
		success : function(backdata) {
			$.messager.alert("提示", backdata.msg);
			menuListFlush();
		},
		error : function(xhr, msg, event) {
			$.messager.alert("提示", msg);
		}
	});
}

/**
 * 打开菜单页面
 * @param {Object} falg
 * @param {Object} id
 */
function openMenuPage(falg, id) {

	var title = "";
	var href = "";
	if ("add" == falg) {
		title = "新增";
		href = _ctx +"/system/menu/info?id=";
	} else if ("edit" == falg) {
		title = "修改";
		href = _ctx +"/system/menu/info?id=" + id;
	} else {
		alert("非法操作！");
		return false;
	}

	$("<div id='menu_popup_div'></div>").dialog({
		title: "菜单【" + title + "】",
		maximizable: false,
		resizable: false,
		width: 600,
		height: 400,
		closed: false,
		cache: false,
		href: href,
		modal: true,
		onLoad: function() {
			$("#menu_info_page_flag").val(falg);
		},
		onClose: function(){
			$("#menu_popup_div").remove();
		},
		buttons: [{
			text: "保存",
			iconCls: "icon-save",
			handler: function() {
				saveMenu();
			}
		}, {
			text: "取消",
			iconCls: "icon-cancel",
			handler: function() {
				$.messager.confirm('Confirm', "还未提交保存,真的要关闭?", function(r){
					if (r) {
						$("#menu_popup_div").dialog("close");
					}
				});
				
			}
		}]
	});
}

/**
 * 保存
 */
function saveMenu() {

	$('#menu_info_form').form({
		type : "POST",
		ajax : true,
		url : _ctx + "/system/menu/save",
		async : false,
		onSubmit : function() {
			// 使用ez自带验证插件
			var bool = $(this).form('enableValidation').form('validate');
			console.log("ez表单验证结果："+bool);
			
			// 使用$验证插件
			var isPass = $('#menu_info_form').valid();
			console.log("$插件验证结果："+isPass);
			if(!isPass) return false;
				
			return bool;
		},
		success : function(bd) {
			var data = $.parseJSON(bd);
			$.messager.alert('消息', data.msg);
			$("#menu_popup_div").dialog("close");
			menuListFlush();
		}
	});
	$('#menu_info_form').submit();
	
}


/**
 * 检索按钮
 */
function SearchMenu() {

	menuListFlush();
}

/**
 * 重置按钮
 */
function ResetMenu() {

	$("#condition_menu_id_input").val('');
	$("#condition_menu_name_input").val('');
}

function menuListFlush() {
	$menu_list_table.datagrid("load", {
		"id": $("#condition_menu_id_input").val(),
		"menuName": $("#condition_menu_name_input").val()
	});
}


function getParent(value, row, index ){
//	alert(value);
//	if(row){
//		return row.parent.name;
//	}else{
//		return '';
//	}
	
}


function serializSearchForm() {
	var jsonSearch = $("#menu_search_condition_form :input").serializeArray();

	// 序列化方式失败!
	// var jsonSearch = $("#account_type_search_form input").serialize();
	// alert(jsonSearch);

	return jsonSearch;
}

/**
 * 打开树形对话框
 * @returns
 */
function showDialog() {

	$("<div id='menu_tree_popup_div'></div>").dialog({
		title : "树形菜单",
		maximizable : true,
		resizable : true,
		width : 400,
		height : 400,
		closed : false,
		cache : false,
		href : _ctx + "/system/menu/tree",
		modal : true,
		onLoad : function() {
		},
		onClose: function(){
			// 务必移除
			$("#menu_tree_popup_div").remove();
		},
		buttons : [ {
			text : "确认",
			iconCls : "icon-save",
			handler : function() {
				
				// 获取选中记录
				var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
				var nodes = treeObj.getCheckedNodes();
				console.log(nodes);
				if(nodes.length != 1){
					$.messager.alert("error", "请选择一个菜单项！");
					return false;
				}else{
					
					console.log( "已选择："+ nodes.length);
					$("#menuParent").textbox('setValue', nodes[0].id);
					$("#menuParent").textbox('setText', nodes[0].name);
					
					console.log(nodes[0].id);
					console.log(nodes[0].name);
				}
				
				$("#menu_tree_popup_div").dialog("close");
			}
		}, {
			text : "关闭",
			iconCls : "icon-cancel",
			handler : function() {
				$("#menu_tree_popup_div").dialog("close");
			}
		} ]
	});
	
}

/**
 * 打开树形对话框
 * @returns
 */
function showEasyTableDialog() {

	$("<div id='menu_tree_popup_div'></div>").dialog({
		title : "树形菜单",
		maximizable : true,
		resizable : true,
		width : 800,
		height : 600,
		closed : false,
		cache : false,
		href : _ctx + "/system/menu/easyTable",
		modal : true,
		onLoad : function() {
		},
		onClose: function(){
			// 务必移除
			$("#menu_tree_popup_div").remove();
		},
		buttons : [ {
			text : "确认",
			iconCls : "icon-save",
			handler : function() {
				
				// 获取选中记录
				var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
				var nodes = treeObj.getCheckedNodes();
				console.log(nodes);
				if(nodes.length != 1){
					$.messager.alert("error", "请选择一个菜单项！");
					return false;
				}else{
					
					console.log( "已选择："+ nodes.length);
					$("#menuParent").textbox('setValue', nodes[0].id);
					$("#menuParent").textbox('setText', nodes[0].name);
					
					console.log(nodes[0].id);
					console.log(nodes[0].name);
				}
				
				$("#menu_tree_popup_div").dialog("close");
			}
		}, {
			text : "关闭",
			iconCls : "icon-cancel",
			handler : function() {
				$("#menu_tree_popup_div").dialog("close");
			}
		} ]
	});
	
}


